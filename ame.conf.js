module.exports = {

  // Nome do seu mini app
  name: "Components Tests",

  // Titulo do mini app, que vai em baixo do botao na pagina de transacoes
  title: "Miniapp de teste de components",

  // Identificador amigavel para colocar em uma url
  slug: "app-teste",

  // Versao do mini app
  version: "0.0.1",

  // Dados da sua empresa
  organization: {
      name: "Calindra"
  },

  // Chave para gerar a ordem/qrcode de pagamento (opcional)
  // "public-key": "uias87891d2d2d2d232d3d2",

  // Versao dos componentes
  "ame-miniapp-components": "2.16.0",

  // Versao do SDK
  "ame-super-app-client": "2.15.0",
}