<Window>
  <View>
    <Header size='xl'>Image Flat Card</Header>
    <Spacing />
    <Subtitle>Card para exibir conteúdo com imagem em grid</Subtitle>
    <Spacing size='md' />
    <Paragraph>Abaixo um exemplo do grid com 5 cards</Paragraph>
    <Spacing size='md' />
    {this.state.card !== null && (
      <View>
        <Paragraph dataCy='state-image-flat-card'>
          Clique no Card {this.state.card + 1}
        </Paragraph>
        <Spacing />
      </View>
    )}
    <ImageFlatCard onClick={index => this.setCard(index)} content={this.arrayCards} />
  </View>
  <View>
      <Spacing />
      <TextLink
          text='Link para documentação'
          onClick={() => {
          window.open('https://ame-miniapp-components.calindra.com.br/docs/imageflatcard/')
          }}
      />
    </View>
  </Window>