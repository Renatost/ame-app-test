<Window>
  <View>
    <Header size="display">Bullet</Header>
    <Spacing />
    <Paragraph>Veja abaixo o componente</Paragraph>
    <Spacing />
    <Spacing />
    <Bullet>Parcele suas compras</Bullet>
    <Bullet>Zero anuidade ou mensalidades</Bullet>
    <Bullet>Compre em +36 milhões de estabelecimentos</Bullet>
    <Bullet>Acumule pontos e participe do clube de compras</Bullet>
    <Spacing />
    <Bullet flat>Parcele suas compras</Bullet>
    <Bullet flat>Zero anuidade ou mensalidades</Bullet>
    <Bullet flat>Compre em +36 milhões de estabelecimentos</Bullet>
    <Bullet flat fontSize="micro" lineHeight="2" color='amecolor-primary-medium'>
      Acumule pontos e participe do clube de compras
    </Bullet>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/bullet/"
        );
      }}
    />
  </View>
</Window>;
