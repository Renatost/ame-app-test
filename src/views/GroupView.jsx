<Window>
    <Group>
        <View>
            <Header>Group</Header>
            <Spacing size={'xxxs'} />
            <Paragraph>Este é um teste com o componente Group</Paragraph>
        </View>
        <View>
            <Paragraph>
                Usado para organização. Possibilida a junção de diversos conteúdos.
            </Paragraph>
        </View>
        <View>
            {
                this.state.showImage ?
                <Illustration
                    height={200}
                    image={require('../assets/pictures/illustration.png')}/>
                : null
            }
            <OptIn 
                onChange={e => this.setState({showImage: e})}
                text={'Exibir Illustration'}/>
        </View>
    </Group>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => window.open('https://ame-miniapp-components.calindra.com.br/docs/group/')}
        />
    </View>
</Window>