export default class AccordionExample extends React.Component {
  accordionData = [
    {
      title: "ATRASEI O PAGAMENTO, O QUE FAZER?",
      items: [
        { title: "Lorem ipsum", content: "Lorem ipsum dolor sit amet" },
        { title: "Lorem ipsum Dolor", content: "Lorem ipsum dolor sit amet" },
      ],
    },
    {
      title: "Seguro celular",
      items: [
        { title: "Lorem ipsum", content: "Lorem ipsum dolor sit amet" },
        { title: "Lorem ipsum Dolor", content: "Lorem ipsum dolor sit amet" },
      ],
    },
    {
      title: "Accordion sem subitens",
      content: "Lorem ipsum dolor sit amet",
    },
  ];
}
