export default class DestinationExample extends React.Component {
  state = {
    origin: "",
    destiny: "",
  };

  onChangeOrigin = (text) => {
    this.setState({ origin: text });
    console.log(this.state);
  };

  onChangeDestiny = (text) => {
    this.setState({ destiny: text });
    console.log(this.state);
  };
}
