export default class HintView {
  state = {
    show: false,
  };
  componentDidMount = () => {
    setTimeout(() => {
      this.setState({
        show: true,
      });
    }, 1000);
  };
}
