

const cursos = [
    {
        key: '1',
        title: 'Curso de React',
        subtitle: 'R$ 500,00',
        promotion: '10%',
        image: 'https://i.pinimg.com/originals/69/16/e2/6916e2b11f37661b5ba69a422c49460c.png',
    },
    {
        key: '2',
        title: 'Curso de React Native',
        subtitle: 'R$ 500,00',
        promotion: '20%',
        image: 'https://i.pinimg.com/600x315/45/80/7d/45807d94375a103fb793b0218aafbee1.jpg',
    },
    {
        key: '3',
        title: 'Curso de JavaScript',
        subtitle: 'R$ 800,00',
        promotion: '30%',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/400px-Unofficial_JavaScript_logo_2.svg.png',
    }
]

const pagamentos = [
    {
      key: '1',
      title: 'Ame',
      icon: require('../assets/pictures/logo-ame.png'),
    },
    {
      key: '2',
      title: 'Cartão',
      icon: 'credit-card-ame',
    },
    {
      key: '3',
      title: 'Dinheiro',
      icon: 'money-outline',
    },
  ];

  const onClick = () => {
      console.log('Click')
  }