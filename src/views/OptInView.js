export default class OptInTest {
    constructor(props) {
        super(props);
        this.state = {
          checked: false,
        };
      }
    
      onChangeHandler = e => {
        this.setState({ checked: !this.state.checked }, () => {
          console.log(
            e
              ? 'Switch ON (valor retornado: ' + e + ')'
              : 'Switch OFF (valor retornado: ' + e + ')',
          );
        });
      };
}