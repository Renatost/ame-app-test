<Window>
    <View>
        <Header size='large'>Componente Information</Header>
        <Spacing />
        <Paragraph>Um componente alerta </Paragraph>
        <Spacing />
        <Information display={this.state.value} onClose={this.handleRender}>
          <Paragraph>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </Paragraph>
        </Information>
        <Button
          label={'Open'}
          type='primary'
          onClick={this.handleRender}
        />
    </View>
</Window>