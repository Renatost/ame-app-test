const dogs = [
  {
    key: "1",
    title: "",
    subtitle: "R$ 1.2098,90",
    image: "https://images.dog.ceo/breeds/hound-ibizan/n02091244_1938.jpg",
  },
  {
    key: "2",
    title: "",
    subtitle: "R$ 1.1928,90",
    image: "https://images.dog.ceo/breeds/hound-afghan/n02088094_1270.jpg",
  },
  {
    key: "3",
    title: "",
    subtitle: "R$ 2.4100,00",
    image: "https://images.dog.ceo/breeds/hound-ibizan/n02091244_1714.jpg",
  },
];

const cats = [
  {
    key: "1",
    title: "",
    subtitle: "R$ 1.1098,90",
    image: "https://cdn2.thecatapi.com/images/b4paC3RGM.jpg",
  },
  {
    key: "2",
    title: "",
    subtitle: "R$ 1.1928,90",
    image: "https://cdn2.thecatapi.com/images/25a.jpg",
  },
  {
    key: "3",
    title: "",
    subtitle: "R$ 2.1100,00",
    image: "https://cdn2.thecatapi.com/images/3hb.jpg",
  },
];

export default class SegmentedControl {
  teste = (index) => {
    console.log(index);
  };
}
