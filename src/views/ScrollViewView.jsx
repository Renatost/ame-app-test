<Window>
  <View paddingHorizontal={20}>
    <Header>ScrollView</Header>
    <Paragraph>Utilizado para rolagem de elementos na tela.</Paragraph>
    <OptIn onChange={this.onChangeHandler} />
    <View height={230}>
      <ScrollView snap scrollTo={2}>
        <Banner height={200}
          images={[
            "https://fotografiamais.com.br/wp-content/uploads/2018/08/fotos-de-paisagem-fotografia-de-paisagem.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://previews.123rf.com/images/yannikap/yannikap1710/yannikap171000056/88372716-beautiful-winter-landscape-with-forest-trees-and-sunrise-winterly-morning-of-a-new-day-purple-winter.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://images.pexels.com/photos/306825/pexels-photo-306825.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
          ]}
        />

        <Banner height={200}
          images={[
            "https://fotografiamais.com.br/wp-content/uploads/2018/08/fotos-de-paisagem-fotografia-de-paisagem.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://previews.123rf.com/images/yannikap/yannikap1710/yannikap171000056/88372716-beautiful-winter-landscape-with-forest-trees-and-sunrise-winterly-morning-of-a-new-day-purple-winter.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://images.pexels.com/photos/306825/pexels-photo-306825.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
          ]}
        />
      </ScrollView>
    </View>
    {/* <View height={250} snap>
      <ScrollView snap hideScroll scrollTo={1} >
        <Banner height={200}
          images={[
            "https://www.ocasaldafoto.com/wp-content/uploads/2018/09/Foto-de-Paisagem-Lago-da-Pampulha-Belo-Horizonte-Charles-Torres.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://previews.123rf.com/images/yannikap/yannikap1710/yannikap171000056/88372716-beautiful-winter-landscape-with-forest-trees-and-sunrise-winterly-morning-of-a-new-day-purple-winter.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://images.pexels.com/photos/306825/pexels-photo-306825.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
          ]}
        />

        <Banner height={200}
          images={[
            "https://www.ocasaldafoto.com/wp-content/uploads/2018/09/Foto-de-Paisagem-Lago-da-Pampulha-Belo-Horizonte-Charles-Torres.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://previews.123rf.com/images/yannikap/yannikap1710/yannikap171000056/88372716-beautiful-winter-landscape-with-forest-trees-and-sunrise-winterly-morning-of-a-new-day-purple-winter.jpg",
          ]}
        />

        <Banner height={200}
          images={[
            "https://images.pexels.com/photos/306825/pexels-photo-306825.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
          ]}
        />
      </ScrollView>
    </View> */}
  </View>
</Window>;
