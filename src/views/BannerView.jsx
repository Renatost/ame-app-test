<Window>
    <Group>
        <View>
            <View>
            <Illustration
                image={require('../assets/pictures/illustration.png')}
                height={200}
            />
            </View>
            <Spacing size='md' />
            <Header size='large'>Lista de Banners</Header>
            <Spacing size='xs' />
            <Banner
                onClick={e => this.setState({ clickBanner: e })}
                images={[
                require('../assets/pictures/crie-um-mini-app.jpg'),
                ]}
            />
            <Spacing size='xs' />
            <Banner
                onClick={e => this.setState({ clickBanner: e })}
                dataCy='banner-2-imagens'
                images={[
                require('../assets/pictures/crie-um-mini-app.jpg'),
                require('../assets/pictures/crie-um-mini-app.jpg'),
                ]}
                // height='136'
            />
            <Spacing size='xs' />
            <Banner
                onClick={e => this.setState({ clickBanner: e })}
                images={[
                require('../assets/pictures/crie-um-mini-app.jpg'),
                require('../assets/pictures/crie-um-mini-app.jpg'),
                ]}
                disabled
            />
        </View>
    </Group>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/banner/')
            }}
        />
    </View>
</Window>