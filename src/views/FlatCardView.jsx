<Window>
  <Group>
    <View>
      <Header size="large">Lista de Flat Card</Header>
      <Spacing />
      <Grid>
        <FlatCard
          onClick={() => console.log("FlatCard Large")}
          size={"large"}
          price={100000}
          conditionPayment={"em até 12x parcelas"}
          paymentshape={"Jeitto"}
          button="Ver Detalhes"
        />
        <FlatCard
          onClick={() => console.log("FlatCard Large")}
          size={"large"}
          price={50000}
          conditionPayment={"em até 12x parcelas"}
          paymentshape={"Jeitto"}
          button="Ver Detalhes"
        />
      </Grid>
      <Spacing />
      <Grid>
        <FlatCard
          disabled
          size={"large"}
          price={100000}
          conditionPayment={"em até 12x parcelas"}
          paymentshape={"Jeitto"}
          button={"Ver Detalhes"}
        />
        <FlatCard
          disabled
          size={"large"}
          price={100000}
          conditionPayment={"em até 12x parcelas"}
          paymentshape={"Jeitto"}
          button={"Ver Detalhes"}
        />
      </Grid>
      <Spacing />
      <FlatCard
        size={"small"}
        price={100000}
        priceProps={{color: 'categorycolor-success'}}
        conditionPayment={"em até 12x parcelas"}
        rate={"Juros: 4,8%"}
        onClick={() => console.log("FlatCard Small")}
      />
      <Spacing />
      <Grid>
        <FlatCard
          disabled
          size={"small"}
          price={100000}
          conditionPayment={"em até 12x parcelas"}
          rate={"Juros: 4,8%"}
        />
        <FlatCard
          disabled
          size={"small"}
          price={100000}
          conditionPayment={"em até 12x parcelas"}
          rate={"Juros: 4,8%"}
        />
      </Grid>
    </View>
  </Group>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/flatCard/"
        );
      }}
    />
  </View>
</Window>;
