<Window>
  <Group>
    <View>
      <Header size="large">Lista de Cards</Header>
      <Spacing />
      <Grid>
        {[
          <Circle
            key={1}
            lagel={"teste"}
            image={"https://placekitten.com/300/300"}
            disabled={true}
          />,
          <Circle key={2} image={"https://placekitten.com/300/300"} />,
          <Circle key={3} icon={require("../assets/svg/icon_ame.svg")} />,
          <Circle key={4} icon={require("../assets/svg/icon_ame.svg")} />,
        ]}
      </Grid>
      <Spacing />
      <Grid>
        {[
          <Circle
            key={1}
            lagel={"teste"}
            image={"https://placekitten.com/300/300"}
            disabled={true}
          />,
          <Circle key={2} image={"https://placekitten.com/300/300"} />,
          <Circle key={3} icon={require("../assets/svg/icon_ame.svg")} />,
        ]}
      </Grid>
      <Spacing />
      <Card
        rightIcon="right-next"
        rightIconColor='red'
        leftIcon={require('../assets/svg/icon_ame.svg')}
        title={"Título do Card"}
        description="descrição"
        subdescription="10% de cashback"
      />
      <Spacing />
      <Card
        rightIcon="bell-notification-outline"
        rightIconColor='pluscolor-secondary-light'
        rightIconClick={e => console.log('ok')}
        leftIcon={require('../assets/pictures/logo-ame.png')}
        title={"Título do Card"}
      />
      <Spacing />
      <Grid>
        <Card
          rightIcon={require("../assets/svg/Seta.svg")}
          title={"Título do Card"}
        />
        <Card
          rightIcon={require("../assets/svg/Seta.svg")}
          title={"Título do Card"}
        />
      </Grid>
      <Spacing />
      <Card
        disabled
        leftIcon="ame-solid"
        title={"Título do Card"}
        description={"Informação complementar do card"}
      />
      <Spacing />
      <Card
        borderColor="amecolor-primary-darkest"
        leftIcon="ame-solid"
        leftIconColor='red'
        title={"Título do Card"}
        description={"Informação complementar do [b]card[/b]"}
      />
      <Spacing />
      <Card
        image={require('../assets/pictures/banner.png')}
        title={"Título do Card"}
      />
      <Spacing />
      <Card
        disabled
        image={require("../assets/pictures/logo_phone.png")}
        title={"Título do Card"}
      />
      <Spacing />
      <Card
        image={require("../assets/pictures/logo_phone.png")}
        hint={"Informação importante"}
        title={"Título do Card"}
        description={"Informação complementar do card"}
      />
    </View>
  </Group>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/card/"
        );
      }}
    />
  </View>
</Window>;
