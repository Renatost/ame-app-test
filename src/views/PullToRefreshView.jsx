<Window>
    <PullToRefreshComponent onRefresh={this.setRefresh}>
    <View>
        <Header size='large'>PullToRefresh</Header>
        <Subtitle>Componentes v2.0</Subtitle>
        <Illustration
        height={300}
        image={require('../assets/svg/icon_ame.svg')}
        />
        <Spacing size='xs' />
        <Paragraph textAlign={'center'}>
        Nossa biblioteca de componentes está mudando, estamos criando novos
        componentes para auxiliar você.
        </Paragraph>
        <Spacing size='xs' />
        <Paragraph>
        Todos eles poderão ser utilizados livremente para composição de suas
        telas.
        </Paragraph>
        <Spacing />
    </View>
    </PullToRefreshComponent>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/pullToRefresh/')
            }}
        />
    </View>
</Window>