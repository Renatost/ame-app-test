<Window>
  <View>
    <View paddingHorizontal={20}>
      <Header fontSize={"large"} marginBottom={0}>
        Mini-app Debugger
      </Header>
      <Header fontSize={"md"}>Video</Header>
      <View>
        <Paragraph size={"sm"}>Para exibir vídeos na plataforma.</Paragraph>
        <Video
          source={"https://s3.amazonaws.com/calindra.com.br/ame.mp4"}
          autoPlay
          thumbnail={this.state.thumb}
          muted
          width={"80%"}
        />
      </View>
      <View>
        <View >
        <Paragraph>Um exemplo com vídeo do Youtube.</Paragraph>
          <Video
            source={"https://www.youtube.com/watch?v=rvM1jOX_A9A"}
            controls
            muted
          />
        </View>
      </View>
    </View>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/video/"
        );
      }}
    />
  </View>
</Window>;
