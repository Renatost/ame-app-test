export default class OptInView {
    state = {
        good: false,
        cheap: false,
        fast: false,
        button: false
    }

    handleGood = () => {
        this.setState( { button: true } )
        if ( this.state.cheap && this.state.fast ) {
            this.setState( { good: true, fast: false } )
        } else {
            this.setState( { good: true } )
        }
    }

    handleCheap = () => {
        this.setState( { button: true } )
        if ( this.state.good && this.state.fast ) {
            this.setState( { cheap: true, good: false } )
        } else {
            this.setState( { cheap: true } )
        }
    }

    handleFast = () => {
        this.setState( { button: true } )
        if ( this.state.good && this.state.cheap ) {
            this.setState( { fast: true, cheap: false } )
        } else {
            this.setState( { fast: true } )
        }
    }

    handleTurnOffAll = () => {
        let { good, cheap, fast } = this.state
        good || cheap || fast ?
            this.setState( { good: false, cheap: false, fast: false, button: false } )
            : this.setState( { button: false } )
    }



}