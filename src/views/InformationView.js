export default class InformationTest {
    constructor() {
        super();
        this.state = {
          value: false,
        };
      }
    
      handleRender = () => {
        this.setState({ value: !this.state.value });
      };
    
}