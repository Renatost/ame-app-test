import Ame from "ame-super-app-client";

export default class InfiniteScrollComponent {
  state = {
    page: 0,
    pokemons: [],
  };

  componentDidMount() {
    Ame.http.get("https://pokeapi.co/api/v2/pokemon/?offset=0").then((res) => {
      this.setState({
        pokemons: res.data.results,
        page: 20,
      });
    });
  }
  getNextPokemons = async () => {
    try {
      const pokemons = await Ame.http.get(
        `https://pokeapi.co/api/v2/pokemon/?offset=${this.state.page}`
      );
      this.setState({ page: this.state.page + 20 });
      return pokemons.data.results;
    } catch (err) {
      console.error(err);
    }
  };

  consoleName(name) {
    console.log(name);
  }

  generateItem = (item, index) => (
    // <View key={`item_${index}`}>
    //   <Paragraph dataCy="infiniteScroll-item">{`${index + 1} - ${
    //     item.name
    //   }`}</Paragraph>
    // </View>
    <Card key={index} title={item.name} onClick={this.consoleName} />
  );
}
