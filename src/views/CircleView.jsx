<Window>
<Group>
        <View>
          <Header size='display'>Circle</Header>
          <Subtitle>Botões de 1/ de tela.</Subtitle>
          <Illustration
            height={300}
            image={require('../assets/pictures/ilustra-1.png')}
          />
          <Spacing />
          <Paragraph>Veja abaixo um exemplo de como eles funcionam.</Paragraph>
        </View>
        <Divisor />
        <View>
          <Header>Uma linha inteira</Header>
          <Spacing />
          <Grid>
            <Circle
              onClick={() => console.log('Circle')}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              onClick={() => console.log('Circle')}
              hint={'NOVO'}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              onClick={() => console.log('Circle')}
              hint={'Em breve'}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              onClick={() => console.log('Circle')}
              label={'Lorem Ipsum'}
              image={require('../assets/pictures/logo_phone.png')}
            />
          </Grid>
        </View>
        <Divisor />
        <View>
          <Header>A mesma linha inativa</Header>
          <Spacing />
          <Grid>
            <Circle
              disabled
              onClick={() => console.log('Circle')}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              disabled
              onClick={() => console.log('Circle')}
              hint={'NOVO'}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              disabled
              onClick={() => console.log('Circle')}
              hint={'Em breve'}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              disabled
              onClick={() => console.log('Circle')}
              label={'Lorem Ipsum'}
              image={require('../assets/pictures/logo_phone.png')}
            />
          </Grid>
        </View>
        <Divisor />
        <View>
          <Header>Linha com 2</Header>
          <Spacing />
          <Grid>
            <Circle
              onClick={() => console.log('Circle')}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              onClick={() => console.log('Circle')}
              hint={'NOVO'}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
          </Grid>
        </View>
        <Divisor />
        <View>
          <Header>Linha com 3</Header>
          <Spacing />
          <Grid>
            <Circle
              onClick={() => console.log('Circle')}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              onClick={() => console.log('Circle')}
              hint={'NOVO'}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
            <Circle
              onClick={() => console.log('Circle')}
              hint={'Em breve'}
              icon={require('../assets/svg/icon_ame.svg')}
              label={'Lorem Ipsum'}
            />
          </Grid>
        </View>
      </Group>
      <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/circle/')
            }}
        />
    </View>
</Window>