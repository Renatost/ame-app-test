<View>
  <View>
  <Header size="display">Radio</Header>
    <Spacing />
    <Paragraph>Radio com flex</Paragraph>
    <Spacing />
    <Radio
      onChange={(e) => {
        console.log(e);
      }}
      name="radio-teste"
      flex
      items={[
        {
          text: "Sim",
          value: "sim",
        },
        {
          text: "Não",
          value: "não",
          checked: true,
        },
      ]}
    />
    <Spacing />
    <Paragraph>Radio sem flex na vertical</Paragraph>
    <Spacing />
    <Radio
      onChange={(e) => {
        console.log(e);
      }}
      name="radio-teste1"
      items={[
        {
          text: "Sim",
          value: "sim",
        },
        {
          text: "Não",
          value: "não",
        },
      ]}
    />
    <Spacing />
    <Paragraph>Radio com 3 opções e flex aparecera um warn.</Paragraph>
    <Spacing />
    <Radio
      onChange={(e) => {
        console.log(e);
      }}
      name="radio-teste2"
      flex
      items={[
        {
          text: "Sim",
          value: "teste",
        },
        {
          text: "Não",
          value: "teste 1",
          checked: true,
        },
        {
          text: "Não",
          value: "teste 1",
          checked: true,
        },
      ]}
    />
  </View>
</View>;
