<Window>
    <View>
        <View paddingHorizontal={20}>
            <Header fontSize={24} marginBottom={0}>Mini-app Debugger</Header>
            <Header fontSize={22}>TimePicker</Header>
            <Spacing />
            <Paragraph fontSize={18}>Agora é a hora de testar o TimePciker.</Paragraph>
            <Spacing size='xs' />
            <TimePicker
                onChange={e => { this.setState({time: e}) }}
                value={this.state.time}
                minTime={this.state.minTime}
                maxTime={this.state.maxTime}
            />
            
            <Spacing />

            <Paragraph>Hora selecionada: {this.state.time}</Paragraph>
           
        </View>
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/timePicker/')
            }}
        />
    </View>
</Window>
