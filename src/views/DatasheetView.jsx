<Window background={"amecolor-primary-light"}>
  <View>
    <Header>DataSheet</Header>
    <Spacing size='sm'/>
    <Datasheet
      title={{
        text: "[b]Olá Mundo[/b]",
        size: "md",
        path: require("../assets/svg/icon_ame.svg"),
      }}
    >
      <Datasheet.Content>
        <Paragraph
          dataCy="datasheet-first-content"
          size={"xs"}
          color={"neutralcolor-darkest"}
        >
          [b]Valor:[/b] R$ 888,88 em 12x de R% 88,88
        </Paragraph>
        <Spacing />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Data do pedido:[/b] 12/12/12
        </Paragraph>
        <Spacing />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Número do protocolo:[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          888888888888888
        </Paragraph>
        <Spacing />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Imóvel:[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Rua Maranga 88
        </Paragraph>
        <Spacing />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Apartamento 88
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Praça Seca
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Rio de Janeiro - RJ
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Coberturas[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Incêndio, queda de raios ou explosão[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Cobertura até: R$250.000,00
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Danos elétricos[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Cobertura até: R$250.000,00
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Perda de pagamento de aluguel[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Cobertura até: R$250.000,00
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Responsabilidade civil familiar[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Cobertura até: R$250.000,00
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Assistência Básica 24h[/b]
        </Paragraph>
        <Spacing size={"nano"} />
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          Chaveiro, eletricista, encanador
        </Paragraph>
        <Spacing />
      </Datasheet.Content>

      <Datasheet.TouchableContent onClick={() => console.log("touchable")}>
        <AmeIcon icon={"receipt-outline"} />
        <Paragraph size={"md"} color={"neutralcolor-darkest"}>
          [b]Apólice do seguro[/b]
        </Paragraph>
      </Datasheet.TouchableContent>

      <Datasheet.Footer>
        <Paragraph size={"xs"} color={"neutralcolor-darkest"}>
          [b]Contato: [/b] 0800-709-8059
        </Paragraph>
        <Datasheet.Button onClick={() => console.log("button")}>
          <AmeIcon icon={"ame-outline"} />
          <Paragraph size={"sm"} color={"neutralcolor-darkest"}>
            Siga para a página do SAC
          </Paragraph>
        </Datasheet.Button>
      </Datasheet.Footer>
    </Datasheet>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/datasheet/"
        );
      }}
    />
  </View>
</Window>;
