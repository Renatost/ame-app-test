  export default class MapTest extends React.Component {
    state = {
      center: {
        lat: -22.94980821,
        lng: -43.19095194,
      },
      zoom: 12,
      markers: [
        {
          lat: -22.970722,
          lng: -43.182365,
          options: {
            title: 'Copacabana',
            draggable: true,
            clickable: false,
            icon:
              'https://developers.google.com/maps/documentation/javascript/examples/full/images/parking_lot_maps.png',
          },
        },
        {
          lat: -22.9511,
          lng: -43.1809,
          options: {
            title: 'Botafogo',
            draggable: true,
          },
        },
      ],
      map: '',
    };
  
    onMarkerPressHandler = item => {
      console.log('onMarkerPress Clicked', item);
    };

}