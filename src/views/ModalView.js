export default class ModalView {
    state = {
      showModal: false,
      position: 'center',
    };
    closeModal = () => {
      this.setState({
        showModal: false,
      });
    };
    openModal = position => {
      this.setState({
        showModal: true,
        position: position,
      });
    };
}