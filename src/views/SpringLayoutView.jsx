<Window>
  <Header>SpringLayout</Header>
  <Subtitle>Componente com formatação de tela padrão.</Subtitle>
  <Spacing />
    <SpringLayout
        head={<Paragraph>Cabeçalho</Paragraph>}
        body={
          <Group>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Accusamus, ad aliquam amet animi aut beatae blanditiis consequatur
              dignissimos earum ex fugiat illum nulla placeat quae quos repellat
              tenetur? Dicta, earum?
            </Paragraph>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Accusamus, ad aliquam amet animi aut beatae blanditiis consequatur
              dignissimos earum ex fugiat illum nulla placeat quae quos repellat
              tenetur? Dicta, earum?
            </Paragraph>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Accusamus, ad aliquam amet animi aut beatae blanditiis consequatur
              dignissimos earum ex fugiat illum nulla placeat quae quos repellat
              tenetur? Dicta, earum?
            </Paragraph>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Accusamus, ad aliquam amet animi aut beatae blanditiis consequatur
              dignissimos earum ex fugiat illum nulla placeat quae quos repellat
              tenetur? Dicta, earum?
            </Paragraph>
          </Group>
        }
        footer={<Paragraph>Rodapé</Paragraph>}
      />
      <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/springLayout/')
            }}
        />
    </View>
</Window>