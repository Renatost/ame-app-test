<Window>
      <View>
        <Header size='default'>Slider</Header>
        <Spacing />
        <CurrencyInput
          fontSize={24}
          maxLength={8}
          prefix='R$'
          value={this.state.currencyValue}
          onChange={value => {
            this.setState({currencyValue: Number(value)});
          }}
        />
        <Spacing />
        <Slider
          dataCy='slider'
          minAmount={15000}
          maxAmount={700000}
          value={this.state.currencyValue}
          onChange={value => {
            this.setState({currencyValue: value});
          }}
        />
      </View>
      <View>
        <Header size='default'>Slidercnão monetário</Header>
        <Spacing />
        <Subtitle>{String(this.state.value)}</Subtitle>
        <Spacing />
        <Slider
          minAmount={1}
          maxAmount={100}
          value={this.state.value}
          onChange={value => {
            this.setState({value});
          }}
          monetary={false}
          step={5}
          minLabel='Beneficiario'
          maxLabel='Beneficiarios'
        />
      </View>
    </Window>