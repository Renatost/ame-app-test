<Window>
  <View>
    <View paddingHorizontal={20}>
      <Header fontSize='large' marginBottom={0}>
        Mini-app Debugger
      </Header>
      <Header fontSize="md">Hint</Header>
      <Paragraph fontSize="sm">
        Componente usado para apresentar alertas.{" "}
      </Paragraph>
      <DropdownButton
        placeholder="Selecione um clube de futebol"
        label="Para qual time você torce?"
        optionList={[
          {
            value: "teste",
            label: "Teste",
          },
        ]}
        onChange={(e) => {
          console.log(e);
        }}
        disabled={true}
      />
      <Hint
        status="success"
        text="Retorno de sucesso"
        dataCy="hint-show"
        show={this.state.show}
      />
      <Spacing />
      <Paragraph>Os status são: error, success e warning</Paragraph>
      <Spacing />
      <Hint
        dataCy="hint-error"
        status="error"
        text="Retorno de erro"
        show={true}
      />
      <Spacing />
      <Hint
        dataCy="hint-success"
        status="success"
        text="Retorno de sucesso"
        show={true}
      />
      <Spacing />
      <Hint
        dataCy="hint-warning"
        status="warning"
        text="Retorno de alerta"
        show={true}
      />
    </View>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/hint/"
        );
      }}
    />
  </View>
</Window>;
