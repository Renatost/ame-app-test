export default class CarouselTest {
  state = {
    initialSlide: 0,
  };

  nextSlide = () => {
    this.setState({ initialSlide: this.state.initialSlide + 1 });
  };

  firstSlide = () => {
    this.setState({ initialSlide: 0 });
  };

  renderCard = (index) => (
    <Card
      key={index}
      icon={require("../assets/svg/icon_ame.svg")}
      title={"Título do Card " + index}
      description={"Um card para servir de exemplo"}
    />
  );
}
