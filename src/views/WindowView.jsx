<Window>
    <View>
        <Header>Window</Header>
        <Spacing size='xxs' />
        <Paragraph>
            Este é um teste do componente Window
        </Paragraph>
    </View>
    <Spacing />
    <View align='center'>        
        <Paragraph textAlign='center'>
            É um componente mestre que precisa envolver toda tela.
            Seu uso é [b]obrigatório[/b] e ele deverá ser o primeiro nó de todas as telas.
            Nunca utilize Window dentro de outro Window.
        </Paragraph>
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => window.open('https://ame-miniapp-components.calindra.com.br/docs/window/')}
        />
    </View>
</Window>