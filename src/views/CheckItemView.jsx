<Window>
    <View>
        <Header size='large'>CheckItem</Header>
        <Spacing />
        <Paragraph>Um componente checkbox</Paragraph>
        <Spacing />
        <CheckItem
            onChange={e => {
            this.onChangeHandler(e);
            }}
            checked={this.state.checked}
            text={'Aceito Termos'}
        />
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/checkItem/')
            }}
        />
    </View>
</Window>