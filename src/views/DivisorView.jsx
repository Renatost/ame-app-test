<Window>
    <Spacing />
    <Header>Divisor</Header>
    <Spacing size={'xxs'} />
    <Subtitle>Componente pequeno para separar conteúdos.</Subtitle>
    <Spacing size={'xxs'} />
    <Divisor line />
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/divisor/')
            }}
        />
    </View>
</Window>