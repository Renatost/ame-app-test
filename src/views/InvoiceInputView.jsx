<Window>
  <View>
    <View paddingHorizontal={20}>
      <Header fontSize="large" marginBottom={0}>
        Mini-app Debugger
      </Header>
      <Header fontSize="md">InvoiceInput</Header>
      <Paragraph fontSize="sm">
        Componente para máscara de códigos de barra entre outros.{" "}
      </Paragraph>
      <Paragraph>Abaixo um exemplo com 15 dígitos</Paragraph>
      <Spacing />
      <InvoiceInput
        dataCy="invoice-label"
        placeholder="0000-0000-0000-0000-0000-0000-0000-0000-0000-0000-0000"
        separator="-"
        numberSeparator={4}
        maxlength={44}
        label="Label do invoice"
      />
      <Spacing />
      <Paragraph>Abaixo um exemplo sem label</Paragraph>
      <Spacing />
      <InvoiceInput
        dataCy="invoice-sem-label"
        placeholder="0000-0000-0000-0000-0000-0000-0000-0000-0000-0000-0000"
        separator="-"
        numberSeparator={4}
        maxlength={44}
      />
    </View>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/invoiceInput/"
        );
      }}
    />
  </View>
</Window>;
