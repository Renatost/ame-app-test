const settings = {
    ColorScreensBackground: 'neutralcolor-medium',
    ColorBackgroundCardsCategories: 'neutralcolor-darkest',
    ColorBackgroundCardsPartners: 'neutralcolor-darkest',
    ColorCardTitleCategories: 'neutralcolor-lightest',
    ColorCardTitlePartners: 'neutralcolor-lightest',
    ColorTextCardPartner: 'pluscolor-primary-medium',
    ColorBorderCard: 'neutralcolor-dark',
    ColorParagraph: 'neutralcolor-darkest',
    ColorHintPartner: 'neutralcolor-lightest',
    ColorNavbar: '#C7C7C7',
    ColorNavbarButtons: '#000000',
    StyleBackgroundHtmlBody: '#C7C7C7',
  };

  export default class AdsView {
      
  }