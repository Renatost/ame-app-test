import Ame from 'ame-super-app-client'
import {StyleResolver} from 'ame-miniapp-components'

export default class ButtonView {

    state = {
        count: 0,
        button: '',
        lock: false
    }

    handleButton = (button) => {
        if(button === this.state.button){
            this.setState({
                count: this.state.count + 1,
            })
        } else {
            this.setState({
                count: 1,
                button: button
            })
        }
        
    }

    handleOptIn = (value) => {
        this.setState({
            lock: !value
        })
    }

    componentDidMount() {

    }
}
