<Window>
    <View>
        <View paddingHorizontal={ 20 }>
            <Header fontSize={ 24 } marginBottom={ 0 }>Mini-app Debugger</Header>
            <Header fontSize={ 22 }>OptIn</Header>
            <Paragraph fontSize={ 18 }>Vamos conhecer um pouco mais do OptIn.</Paragraph>
            <Divisor height={ 2 } backgroundColor={ '#e8165a' } />
            <Button block size={ 'small' } disabled={ !this.state.button } onClick={ this.handleTurnOffAll }>
                { this.state.button ? 'Desativar todos' : 'OptIns desativados' }
            </Button>
            <Divisor />
            <Paragraph fontSize={ 25 } fontWeight={ this.state.good ? 900 : 300 }>Bom</Paragraph>
            <OptIn checked={ this.state.good } onChange={ this.handleGood } />
            <Divisor />
            <Paragraph fontSize={ 25 } fontWeight={ this.state.cheap ? 900 : 300 }>Barato</Paragraph>
            <OptIn checked={ this.state.cheap } onChange={ this.handleCheap } />
            <Divisor />
            <Paragraph fontSize={ 25 } fontWeight={ this.state.fast ? 900 : 300 }>Rápido</Paragraph>
            <OptIn checked={ this.state.fast } onChange={ this.handleFast } />
            <Divisor height={ 2 } backgroundColor={ '#e8165a' } />
        </View>
    </View>
</Window>
