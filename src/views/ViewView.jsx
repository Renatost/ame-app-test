<Window>
  <View>
    <Header>Mini-app Debugger</Header>
    <Header>View</Header>
    <Spacing />
    <Paragraph>A View é uma div facilmente estilizável.</Paragraph>
    <Spacing />
    <Divisor />
    <Spacing />
    <View
      background="amecolor-primary-medium"
      border="thick"
      borderRadius="md"
      borderColor="amecolor-secondary-medium"
    >
      <Paragraph color="neutralcolor-light">
        Esta é uma View com borda e background.
      </Paragraph>
    </View>
    <Spacing />
    <Divisor />
    <Spacing />
    <View direction="row" align="center" justify="between">
      <Paragraph>
        Esta é uma View com direction='row', align='center' e justify='between'.
      </Paragraph>
      <Paragraph>Nesta possuímos dois componentes Paragraph.</Paragraph>
    </View>
    <Divisor />
    <View />
  </View>
  <View
  marginX='md'
    width='250px'
    height='300px'
    borderRadiusLeftTop='sm'
  borderRadiusRightTop='sm'
  borderRadiusLeftBottom='sm'
  borderRadiusRightBottom='sm'
    background='amecolor-secondary-darkest'
  />
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/view/"
        );
      }}
    />
  </View>
</Window>;
