<View>
    <Header>Destination</Header>
    <Spacing size='sm' />
    <View background='amegradient-secondary'>
        <Destination
            type='input'
            originInputValue={this.state.origin}
            destinyInputValue={this.state.destiny}
            onChangeOrigin={text => this.onChangeOrigin(text)}
            onChangeDestiny={text => this.onChangeDestiny(text)}
          />
    </View>
    <Spacing size='sm' />
        <Destination
        onClickChangeOrigin={() => alert('origin')}
        onClickChangeDestiny={() => alert('destiny')}
        type='text'
        originAddress='Rua Fidêncio Ramos, 302 - Vila Olimpia, São Paulo - SP, 04551-010'
        destinyAddress='Rua Fidêncio Ramos, 302 - Vila Olimpia, São Paulo - SP, 04551-010'
        />
        <View>
            <Spacing />
            <TextLink
                text='Link para documentação'
                onClick={() => {
                window.open('https://ame-miniapp-components.calindra.com.br/docs/destination/')
                }}
            />
        </View>
</View>