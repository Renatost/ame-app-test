<Window>
  <View>
    <Header>Touchable</Header>
    <Spacing size='md' />
    <Paragraph >
      Componente para criar ação de click em qualquer outro componente
    </Paragraph>
    <Spacing size="md" />
    <Touchable onClick={this.handleClick}>
      <Subtitle>Agora o Subtitle também tem ação de click</Subtitle>
    </Touchable>
    <Spacing size="md" />
    <Touchable disabled disabledOnClick={this.disabledClick}>
      <Animator animationData={require("../assets/svg/dolphin.json")} />
    </Touchable>
  </View>
</Window>;
