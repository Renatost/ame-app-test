<Window>
  <Map
    mapOptions={{
      center: this.state.center,
      zoom: this.state.zoom,
    }}
    onMarkerPress={this.onMarkerPressHandler}
    markers={this.state.markers}
    markerInfo={true}
    getMap={(map) => {
      this.setState({ map });
    }}
  />

  <Button
    label="Add"
    type="secondary"
    onClick={() => {
      const cpMarker = {
        lat: -22.950722,
        lng: -43.172365,
        options: {
          draggable: true,
        },
      };
      this.setState({
        markers: [...this.state.markers, { ...cpMarker }],
      });
    }}
  />

  <Button
    label="Change"
    type="primary"
    onClick={() => {
      const cpMarker = {
        lat: -22.970722,
        lng: -43.182365,
      };
      setInterval(() => {
        cpMarker.lat -= 0.001;
        cpMarker.lng += 0.001;
        this.setState({
          markers: [
            {
              lat: cpMarker.lat,
              lng: cpMarker.lng,
            },
          ],
        });
      }, 500);
    }}
  />

  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open("https://ame-miniapp-components.calindra.com.br/docs/map/");
      }}
    />
  </View>
</Window>;
