<Window>
    <View>
        <Header size='large'>CheckItem</Header>
        <Spacing />
        <Paragraph>Um componente checkbox</Paragraph>
        <Spacing />
        <CheckItem
            onChange={e => {
            this.onChangeHandler(e);
            }}
            checked={this.state.checked}
            text={'Aceito Termos'}
        />
    </View>
</Window>