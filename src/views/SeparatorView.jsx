<Window>
    <View>
        <View paddingHorizontal={ 20 }>
            <Header fontSize={ 24 } marginBottom={ 0 }>Mini-app Debugger</Header>
            <Header fontSize={ 22 }>Separator</Header>
            <Paragraph fontSize={ 18 }>Usado para separação de conteúdo</Paragraph>
            <Divisor
                marginTop={ 10 }
                marginBottom={ 10 }
                backgroundColor={ '#0000ff' }
                height={ 1 }
            />
            <Paragraph>Este texto está separado por um Separator</Paragraph>
            <Divisor
                marginTop={ 10 }
                marginBottom={ 10 }
                backgroundColor={ '#0000ff' }
                height={ 1 }
            />
        </View>
    </View>
</Window>
