<Window>
  <View>
    <View>
      <Header>Image</Header>
      <Paragraph>Usado para exibir uma imagem</Paragraph>
      <Image
        src={require("../assets/pictures/produto.png")}
        borderRadius="circular"
        
      />
      <View flexWrap="wrap">
        <Image icon="ame-solid" colorIcon="categorycolor-waiting" />
        <Image icon="trash-solid" />
        <Image icon="money-solid" colorIcon="categorycolor-waiting" />
        <Image icon="credit-card-ame" />
        <Image
          src={require("../assets/pictures/logo_phone.png")}
          width="50px"
          height="50px"
        />
        <Image icon="ame-solid" colorIcon="amecolor-primary-medium" />
      </View>
    </View>
  </View>
</Window>;
