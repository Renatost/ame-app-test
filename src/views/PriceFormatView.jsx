<Window>
    <View>
        <View paddingHorizontal={20}>
            <Header fontSize={24} marginBottom={0}>Mini-app Debugger</Header>
            <Header fontSize={22}>PriceFormat</Header>
            <Paragraph>Usado para formatar um preço de acordo com a moeda brasileira.</Paragraph>
            <Paragraph fontSize={10}>Para inserir os centavos, basta colocar o ponto (.)</Paragraph>
            <Divisor />
            <Paragraph>Insira o preço no campo abaixo </Paragraph>
            <Input borderWidth={1} borderColor={'#000'} inputMode={'numeric'} onChange={e => this.setState({ price: e })} />
            <Divisor height={10} backgroundColor={'transparent'} />
            <Paragraph>Preço formatado</Paragraph>
            <Paragraph color={'#e8165a'} color={'#'}>
                <PriceFormat amount={this.state.price} />
            </Paragraph>
        </View>
    </View>
</Window>
