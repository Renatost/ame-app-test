<Window>
  <View>
    <Text fontSize="large">Text</Text>
    <Spacing size="md" />
    <Text
      fontSize="xxs"
      fontWeight="bold"
      lineHeight="distant"
      color="amecolor-primary-medium"
      selectable
    >
      Novo componente de [u]Texto[/u] para uso nos miniapps da AME.
    </Text>
    <Text target="title">Titulo</Text>
    <Spacing size="md" />
    <Text fontSize="xxxs" fontWeight="bold" lineHeight="medium">
      3. Declaro estar ciente das profissões sem aceitação.
      <AmeIcon
        icon="help-outline"
        color="red"
        onClick={() => console.log("Click")}
      />
    </Text>
  </View>
</Window>;
