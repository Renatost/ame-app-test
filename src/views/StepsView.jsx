<View>
  <View>
    <Steps
      steps={[
        {
          title: "Cartão entregue",
          checked: false,
        },
        {
          title: "Cartão saiu para entrega",
          checked: false,
        },
        {
          title: "Cartão em trânsito",
          checked: true,
        },
        {
          title: "Cartão aprovado",
          checked: true,
        },
      ]}
    />
  </View>
  <View border="hairline" borderRadius="md">
    <Steps
      steps={[
        {
          title: "Encontre a melhor oferta",
          icon: "search",
          checked: true,
        },
        {
          title: "Enviar fotos suas",
          icon: "camera-outline",
          checked: false,
        },
        {
          title: "Enviar documento",
          icon: "documents-id-outline",
          checked: false,
        },
        {
          title: "Enviar comprovante de endereço*",
          icon: "adress-outline",
          checked: false,
        },
        {
          title: "Enviar comprovante de renda*",
          icon: "money-outline",
          checked: false,
        },
        {
          title: "Assinar contrato digital",
          icon: "extended-warranty",
          checked: false,
        },
      ]}
      icons
    />
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/steps/"
        );
      }}
    />
  </View>
</View>;
