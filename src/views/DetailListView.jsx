<View>
  <Header size="display">DetailList</Header>
  <Spacing size="sm" />
  <Paragraph>Veja abaixo o funcionamento do DetailList</Paragraph>
  <Spacing size="sm" />
  <View>
    <DetailList items={this.items} onClick={this.handleClick} />
  </View>
</View>;
