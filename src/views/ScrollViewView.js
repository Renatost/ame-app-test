export default class ScrollViewView {
    state = {
        showDirection: true
    }

    onChangeHandler = () => {
        this.setState({ showDirection: !this.state.showDirection })
    }

}
