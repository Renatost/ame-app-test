export default class DetailListView {
  handleClick = (e) => {
    console.log(e);
  };
  items = [
    {
      title: "Pedágio Linha Amarela",
      description: "12/06 - 06h10",
      subdescription: "Audi",
      leftIcon: "feature-taggy",
      leftIconColor: "black",
      rightIcon: "right-next",
      rightIconColor: "red",
      tag: "R$19,99",
    },
    {
      title: "Pedágio Rio-NITERÓI",
      description: "12/06 - 09h46",
      subdescription: "Audi",
      leftIcon: "feature-taggy",
      leftIconColor: "black",
      rightIcon: "right-next",
      rightIconColor: "red",
      tag: "R19,99",
    },
    {
      title: "Fatura Taggy - Junho",
      description: "12/06 - 12h02",
      subdescription: "Audi",
      leftIcon: "ame-outline",
      leftIconColor: "red",
      rightIcon: "right-next",
      rightIconColor: "red",
      tag: "R$360,98",
    },
  ];
}
