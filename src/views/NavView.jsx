<Window>
  <Nav
    backgroundColor={"amegradient-primary"}
    iconColor={"amecolor-secondary-light"}
    sticky
    safeArea={true}
    onBackClick={this.goToBack}
    onCloseClick={this.goToClose}
  />
  <Spacing size={"large"} />
  <Illustration
    height={200}
    image={require("../assets/pictures/logo-ame.png")}
  />{" "}
  <Header block textAlign={"center"} fontSize={'large'}>
    Mini-App Component Tester
  </Header>
</Window>;
