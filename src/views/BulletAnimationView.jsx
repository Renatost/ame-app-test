<View>
    <Header size='display'>Bullet Animation</Header>
    <Spacing />
    <BulletAnimation bullets={3} selected={this.state.bulletSelected} />
    <Spacing />
    <Illustration
    height={300}
    image={require('../assets/pictures/ilustra-1.png')}
    />
    <Spacing />
    <Paragraph>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta cumque
    aspernatur eum illo eius accusantium eligendi quidem fugiat ratione
    libero. Sunt iste dicta aut placeat velit? Vero dicta necessitatibus
    aperiam.
    </Paragraph>
    <Spacing />
    <Button
    dataCy='primaryButton'
    // disabled={this.state.bulletSelected === 3 ? true : false}
    label={this.state.bulletSelected === 3 ? 'Entendi' : 'Proximo'}
    type={this.state.bulletSelected === 3 ? 'primary' : 'secondary'}
    onClick={() => {
        this.nextStage();
    }}
    />
    <Spacing />
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/bulletAnimation/')
            }}
        />
    </View>
</View>