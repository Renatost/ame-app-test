<Window>
    <View>
        <View paddingHorizontal={20}>
            <Header fontSize={24} marginBottom={0}>Mini-app Debugger</Header>
            <Header fontSize={22}>View</Header>
            <Paragraph fontSize={18}>Uma view que possui comportamentos semelhantes ao View do HTML.</Paragraph>
            <Divisor />
            <View flexDirection={'column'}>
                <Paragraph color={'#fc6097'}> flexDirection Column</Paragraph>
                <Paragraph color={'#fc6057'}> flexDirection Column</Paragraph>
                <Paragraph color={'#fc6037'}> flexDirection Column</Paragraph>
            </View>
            <Divisor />
            <View flexDirection={'row'}>
                <Paragraph color={'#fc6097'}> flexDirection row</Paragraph>
                <Paragraph color={'#fc6057'}> flexDirection row</Paragraph>
                <Paragraph color={'#fc6037'}> flexDirection row</Paragraph>
            </View>
            <Divisor />
            <View justifyContent={'space-between'} flexWrap={'wrap-reverse'}>
                <Paragraph color={'#f59231'}>View wrap-reverse</Paragraph>
                <Paragraph color={'#cf428d'}> View wrap-reverse</Paragraph>
            </View>
            <Divisor />
            <View justifyContent={'center'} >
                <Paragraph color={'#f59231'}>View justifyContent='center</Paragraph>

            </View>
            <Divisor />
            <View justifyContent={'space-around'} flexWrap={'wrap-reverse'}>
                <Paragraph color={'#f59231'}> Aqui temos mais um FlexWrap</Paragraph>
                <Paragraph color={'#cf428d'}> Aqui temos mais um FlexWrap</Paragraph>
            </View>
            <Divisor />
            <View justifyContent={'space-evenly'}>
                <Paragraph color={'#f59231'}> View space-evenly</Paragraph>
                <Paragraph color={'#cf428d'}> View space-evenly</Paragraph>
            </View>
            <Divisor />
            <View flexGrow={"unset"}>
                <Paragraph color={'#f59231'}> flexGrow unset quebra linha </Paragraph>
                <Paragraph color={'#f59231'}> flexGrow unset quebra linha </Paragraph>
                <Paragraph color={'#f59231'}> flexGrow unset quebra linha </Paragraph>
            </View>
            <Divisor />
            <View flexShrink={2}>
                <Paragraph color={'#f59231'}>flexShrink 4</Paragraph>
                <Paragraph color={'#cf428d'}>flexShrink 4</Paragraph>
                <Paragraph color={'#f59231'}>flexShrink 4</Paragraph>
                <Paragraph color={'#cf428d'}>flexShrink 4</Paragraph>
            </View>

            <View order={2}>
                <Paragraph color={'#f59231'}> View order 2 </Paragraph>
                <Paragraph color={'#cf428d'}> View order 2 </Paragraph>
                <Paragraph color={'#f59221'}> View order 2 </Paragraph>
                <Paragraph color={'#cf428d'}> View order 2 </Paragraph>
            </View>
            <Divisor />
            <View alignContent={"center"}>
                <Paragraph color={'#f59231'}>View alignContent</Paragraph>
                <Paragraph color={'#cf428d'}>View alignContent</Paragraph>
                <Paragraph color={'#f59231'}>View alignContent</Paragraph>

            </View>
            <Divisor />
            <View alignSelf={"stretch"}>
                <Paragraph color={'#f59231'}>View alignSelf</Paragraph>
                <Paragraph color={'#cf428d'}>View alignSelf</Paragraph>
                <Paragraph color={'#f59231'}>View alignSelf</Paragraph>

            </View>
            <View placeContent={"end space-between"}>
                <Paragraph color={'#cf428d'}>View placeContent</Paragraph>
                <Paragraph color={'#f59231'}>View placeContent</Paragraph>
                <Paragraph color={'#f59231'}>View placeContent</Paragraph>
            </View>

            <View placeItems={"start end"}>
                <Paragraph color={'#cf428d'}>View placeItems</Paragraph>
                <Paragraph color={'#f59231'}>View placeItems</Paragraph>
                <Paragraph color={'#f59231'}>View placeItems</Paragraph>

            </View>

            <View rowGap={20}>
                <Paragraph color={'#cf428d'}>View rowGap</Paragraph>
                <Paragraph color={'#f59231'}>View rowGap</Paragraph>
                <Paragraph color={'#cf428d'}>View rowGap</Paragraph>
                {/* <Paragraph color={'#f59231'}>View rowGap</Paragraph>
                <Paragraph color={'#f59231'}>View rowGap</Paragraph> */}
            </View>

            <View columnGap={40}>
                <Paragraph color={'#cf428d'}>View columnGap</Paragraph>
                <Paragraph color={'#f59231'}>View columnGap</Paragraph>
                <Paragraph color={'#cf428d'}>View columnGap</Paragraph>
            </View>

            <View gridGap={"20%"}>
                <Paragraph color={'#cf428d'}>View gridGap 20%</Paragraph>
                <Paragraph color={'#f59231'}>View gridGap 20%</Paragraph>
                <Paragraph color={'#cf428d'}>View gridGap 20%</Paragraph>
                <Paragraph color={'#cf428d'}>View gridGap 20%</Paragraph>
            </View>

            <View gridGap={10}>
                <Paragraph color={'#cf428d'}>View gridGap 10%</Paragraph>
                <Paragraph color={'#f59231'}>View gridGap 10%</Paragraph>
                <Paragraph color={'#cf428d'}>View gridGap 10%</Paragraph>
                <Paragraph color={'#cf428d'}>View gridGap 20%</Paragraph>
            </View>

            <View alignItems={"center"}>
                <Paragraph color={'#cf428d'}>View alignItems</Paragraph>
                <Paragraph color={'#f59231'}>View alignItems</Paragraph>
                <Paragraph color={'#cf428d'}>View alignItems</Paragraph>
                <Paragraph color={'#cf428d'}>View alignItems</Paragraph>
            </View>

            <View flexFlow={"row wrap"}>
                <Paragraph color={'#cf428d'}>View flexFlow</Paragraph>
                <Paragraph color={'#f59231'}>View flexFlow</Paragraph>
                <Paragraph color={'#cf428d'}>View flexFlow</Paragraph>
            </View>

            <View flexBasis={"auto"}>
                <Paragraph color={'#cf428d'}>View flexBasis</Paragraph>
                <Paragraph color={'#f59231'}>View flexBasis</Paragraph>
                <Paragraph color={'#cf428d'}>View flexBasis</Paragraph>
            </View>

            <View flex={'10%'}>
                <Paragraph color={'#cf428d'}>View flex</Paragraph>
                <Paragraph color={'#f59231'}>View flex</Paragraph>
                <Paragraph color={'#cf428d'}>View flex</Paragraph>
                <Paragraph color={'#f59231'}>View flex</Paragraph>
            </View>
        </View>
    </View>
</Window>
