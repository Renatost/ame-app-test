<Window>
    <View>
        <Header>Subtítulos</Header>
        <Spacing size='sm' />
        <Paragraph>
        Usados para descrever textos explicativos ao cabeçalho.
        </Paragraph>
        <Spacing size='sm' />
        <Paragraph color='amecolor-secondary-medium'>
        Subtítulo tamanho LG.
        </Paragraph>
        <Subtitle size='lg'>What does the fox say !!!!!</Subtitle>
        <Spacing size='sm' />
        <Paragraph color='amecolor-secondary-medium'>
        Subtítulo tamanho Default.
        </Paragraph>
        <Subtitle size='default'>What does the fox say !!!!!</Subtitle>
        <Spacing size='sm' />
        <Paragraph color='amecolor-secondary-medium'>
        Subtítulo tamanho MD.
        </Paragraph>
        <Subtitle size='md'>What does the fox say !!!!!</Subtitle>
        <Spacing size='sm' />
        <Paragraph color={'categorycolor-expired'}>
        Subtítulo tamanho SM.
        </Paragraph>
        <Subtitle size='sm'>What does the fox say !!!!!</Subtitle>
        <Spacing size='sm' />
        <Paragraph color='amecolor-secondary-medium'>
        Subtítulo tamanho XS.
        </Paragraph>
        <Subtitle size='xs'>What does the fox say !!!!!</Subtitle>
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/subtitle/')
            }}
        />
    </View>
</Window>