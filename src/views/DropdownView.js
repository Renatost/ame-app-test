export default class DropdownTest {
  state = {
    user: { firstName: "", lastName: ""},
    genderList: [
      {
        label: "Feminino",
        value: "F",
      },
      {
        label: "Masculino",
        value: "M",
      },
    ],
    optionListModal: [
      {
        value: "FLA",
        label: "Flamengo",
        fullName: "Clube de Regatas do Flamengo",
        foundation: "15 de novembro de 1895",
      },
      {
        value: "BOT",
        label: "Botafogo",
        fullName: "Botafogo de Futebol e Regatas",
        foundation: "1 de julho de 1894",
      },
      {
        value: "SPO",
        label: "São Paulo",
        fullName: "São Paulo Futebol Clube",
        foundation: "25 de Janeiro de 1930",
      },
      {
        value: "FLU",
        label: "Fluminense",
        fullName: "Fluminense Football Club",
        foundation: "21 de julho de 1902 ",
      },
      {
        value: "VAS",
        label: "Vasco da Gama",
        fullName: "Club de Regatas Vasco da Gama",
        foundation: "21 de agosto de 1898",
      },
      {
        value: "AMG",
        label: "América-MG",
        fullName: "América Futebol Clube",
        foundation: "30 de abril de 1912",
      },
      {
        value: "FLA",
        label: "Flamengo",
        fullName: "Clube de Regatas do Flamengo",
        foundation: "15 de novembro de 1895",
      },
      {
        value: "BOT",
        label: "Botafogo",
        fullName: "Botafogo de Futebol e Regatas",
        foundation: "1 de julho de 1894",
      },
      {
        value: "SPO",
        label: "São Paulo",
        fullName: "São Paulo Futebol Clube",
        foundation: "25 de Janeiro de 1930",
      },
      {
        value: "FLU",
        label: "Fluminense",
        fullName: "Fluminense Football Club",
        foundation: "21 de julho de 1902 ",
      },
      {
        value: "VAS",
        label: "Vasco da Gama",
        fullName: "Club de Regatas Vasco da Gama",
        foundation: "21 de agosto de 1898",
      },
      {
        value: "AMG",
        label: "América-MG",
        fullName: "América Futebol Clube",
        foundation: "30 de abril de 1912",
      },
      {
        value: "FLA",
        label: "Flamengo",
        fullName: "Clube de Regatas do Flamengo",
        foundation: "15 de novembro de 1895",
      },
      {
        value: "BOT",
        label: "Botafogo",
        fullName: "Botafogo de Futebol e Regatas",
        foundation: "1 de julho de 1894",
      },
      {
        value: "FLU",
        label: "Fluminense",
        fullName: "Fluminense Football Club",
        foundation: "21 de julho de 1902 ",
      },
      {
        value: "VAS",
        label: "Vasco da Gama",
        fullName: "Club de Regatas Vasco da Gama",
        foundation: "21 de agosto de 1898",
      },
      {
        value: "FLA",
        label: "Flamengo",
        fullName: "Clube de Regatas do Flamengo",
        foundation: "15 de novembro de 1895",
      },
      {
        value: "BOT",
        label: "Botafogo",
        fullName: "Botafogo de Futebol e Regatas",
        foundation: "1 de julho de 1894",
      },
      {
        value: "FLU",
        label: "Fluminense",
        fullName: "Fluminense Football Club",
        foundation: "21 de julho de 1902 ",
      },
      {
        value: "VAS",
        label: "Vasco da Gama",
        fullName: "Club de Regatas Vasco da Gama",
        foundation: "21 de agosto de 1898",
      },
    ],
    optionList: [
      {
        value: "FLA",
        label: "Flamengo",
        fullName: "Clube de Regatas do Flamengo",
        foundation: "15 de novembro de 1895",
      },
      {
        value: "BOT",
        label: "Botafogo",
        fullName: "Botafogo de Futebol e Regatas",
        foundation: "1 de julho de 1894",
      },
      {
        value: "FLU",
        label: "Fluminense",
        fullName: "Fluminense Football Club",
        foundation: "21 de julho de 1902 ",
      },
      {
        value: "VAS",
        label: "Vasco da Gama",
        fullName: "Club de Regatas Vasco da Gama",
        foundation: "21 de agosto de 1898",
      },
    ],
  };

  selectChange = (e) => {
    console.log("Selected: ", e);
  };

  // noRefCheck = (item) => {
  //   let user = this.state.user;
  //   user.gender = item.value;
  //   this.setState({ user: user });
  //   console.log(this.state.user);
  // };
}
