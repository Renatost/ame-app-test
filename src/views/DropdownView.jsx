<View>
  <Header size="display">Dropdown</Header>
  <Spacing size="sm" />
  <Paragraph>
    Dropdowm com muitas opções abre o modal com campo de busca.
  </Paragraph>
  <Spacing size="sm" />
  <DropdownButton
    id="muitos"
    nextInputId="menos"
    dataCy="drop-down-modal"
    placeholder="Selecione um clube de futebol"
    label="Para qual time você torce?"
    optionList={this.state.optionListModal}
    value={this.state.optionListModal[3].label}
    onChange={(e) => {
      this.selectChange(e);
    }}
  />
  <Spacing size="sm" />
  <Paragraph>
    Dropdowm com muitas opções e sem campo de busca.
  </Paragraph>
  <Spacing size="sm" />
  <DropdownButton
    id="muitos"
    nextInputId="menos"
    dataCy="drop-down-modal"
    placeholder="Selecione um clube de futebol"
    label="Para qual time você torce?"
    optionList={this.state.optionListModal}
    hideSearch
    onChange={(e) => {
      this.selectChange(e);
    }}
  />
  <Spacing size="sm" />
  <Paragraph>Dropdowm com poucas opções abre abaixo do input.</Paragraph>
  <Spacing size="sm" />
  <DropdownButton
    id="menos"
    nextInputId="input"
    dataCy="drop-down-menor"
    label="Para qual time você torce?"
    placeholder="Selecione um clube de futebol"
    optionList={this.state.optionList}
    onChange={(e) => {
      this.selectChange(e);
    }}
  />
  <Spacing size="sm" />
  <Input
    id="input"
    nextInputId="gender"
    onChange={function notRefCheck(item) {
      let user = this.state.user;
      user.firstName = item;
      this.setState({ user: user });
      console.log(this.state.user);
    }.bind(this)}
    type="text"
    inputMode="text"
    label="Input"
  />
  <Spacing size="sm" />
  <Paragraph>Dropdowm desabilitado.</Paragraph>
  <Spacing size="sm" />
  <DropdownButton
    placeholder="Selecione um clube de futebol"
    label="Para qual time você torce?"
    optionList={this.state.optionList}
    value={this.state.optionList[0].label}
    onChange={(e) => {
      this.selectChange(e);
    }}
    disabled={true}
  />
  <Spacing size="md" />
  <Spacing size="md" />
  <DropdownButton
  id="gender"
    label="Sexo"
    onChange={function notRefCheck(item) {
      let user = this.state.user;
      user.gender = item.value;
      this.setState({ user: user });
      console.log(this.state.user);
    }.bind(this)}
    optionList={this.state.genderList}
    // value={this.state.user.gender}
    placeholder="Escolha uma opção"
  />
  <Spacing size="md" />

  <Paragraph>
    Botão para trocar o segundo Dropdown para testar se volta para o placeholder
  </Paragraph>
  <Spacing size="md" />
  <Button
    label="trocar lista"
    type="primary"
    dataCy="botao-troca-lista"
    onClick={() => {
      this.setState({
        optionListDefault: [
          {
            value: "FLA",
            label: "Flamengo",
            fullName: "Clube de Regatas do Flamengo",
            foundation: "15 de novembro de 1895",
          },
        ],
      });
    }}
  />
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/dropdownButton/"
        );
      }}
    />
  </View>
</View>;
