export default class BulletAnimationExample {
    state = {
      bulletSelected: 1
    }
  
    nextStage() {
      if (this.state.bulletSelected < 3) {
        this.setState({bulletSelected: this.state.bulletSelected + 1});
      }
      console.log(this.state.bulletSelected);
    }
}