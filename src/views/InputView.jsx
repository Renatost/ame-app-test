<Group>
  <View>
    <Header size="display">Inputs</Header>
    <Spacing />
    <Paragraph>Input Padrão</Paragraph>
    <Spacing />
    <Input
      placeholder="Preencha aqui"
      disabled
      value="Teste disabled"
      label="Teste"
    />
  </View>
  <Divisor />
  <View>
    <Spacing />
    <Spacing />
    <Paragraph>Input com máscara e inputMode numeric</Paragraph>
    <Spacing />
    <Input inputMode={"numeric"} mask={"999.999.999-99"} />
  </View>
  <Divisor />
  <View>
    <Spacing />
    <Spacing />
    <Paragraph>Input Numerico</Paragraph>
    <Spacing />
    <Input
      id="numeric"
      nextInputId="email"
      onChange={(e) => console.log(e)}
      onSubmit={() => console.log("next")}
    />
  </View>
  <Divisor />
  <View>
    <Spacing />
    <Spacing />
    <Paragraph>Input email</Paragraph>
    <Spacing />
    <Input
      id="email"
      inputMode={"email"}
      type={"text"}
      mode="danger"
      label="Seu email"
      hint="Preencha corretamente"
      onSubmit={() => console.log("next")}
      nextInputId="search"
    />
  </View>
  <Divisor />
  <View>
    <Spacing />
    <Spacing />
    <Paragraph>Input Search</Paragraph>
    <Spacing />
    <Input
      id="search"
      inputMode="search"
      nextInputId="nome"
      placeholder="Qual serviço você busca?"
      onChange={(e) => this.inputSearch(e)}
      onSubmit={() => console.log("next")}
    />
  </View>
  <Spacing />
  <View>
    <Input
      label="Nome"
      onChange={function noRefCheck(name) {
        let user = this.state.user;
        user.firstName = name;
        this.setState({ user: user });
        console.log(this.state.user)
      }.bind(this)}
      inputMode='text'
      nextInputId="sm"
      placeholder="Seu nome"
      value={this.state.user.firstName}
      onSubmit={this.handleSubmit}
    />
    <Spacing size="sm" />
    <Input
      id="sm"
      label="Sobrenome"
      onChange={function noRefCheck(lastName) {
        let user = this.state.user;
        user.lastName = lastName;
        this.setState({ user: user });
      }.bind(this)}
      placeholder="Sobrenome"
      // value={this.state.user.lastName}
    />

    <Spacing size="sm" />

    <DatePicker
      id="dn"
      label="Data de Nascimento"
      onChange={function noRefCheck(date) {
        let user = this.state.user;
        user.birthDate = date;
        this.setState({ user: user });
      }.bind(this)}
      onSubmit={() => console.log("next")}
      nextInputId="em"
    />

    <Spacing size="sm" />

    <Input
      id="test"
      label="Texto"
      inputMode="numeric"
      type="number"
      nextInputId="drop"
      onSubmit={this.handleSubmit}
    />
    <Spacing size="md" />
    <DropdownButton
      id="drop"
      nextInputId="next"
      placeholder="Selecione um clube de futebol"
      label="Para qual time você torce?"
      optionList={this.state.optionList}
      onChange={(e) => console.log(e)}
    />
    <Input
      id="next"
      label="Email"
      onChange={function noRefCheck(email) {
        let user = this.state.user;
        user.email = email;
        this.setState({ user: user });
      }.bind(this)}
      placeholder="email@seu.com"
      onSubmit={this.handleSubmit}
      type="email"
      nextInputId="tel"
    />
    <Spacing />
  </View>
  <Spacing />
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/input/"
        );
      }}
    />
  </View>
</Group>;
