<View>
  <Header>ExpandableCard</Header>
  <View>
    <ExpandableCard>
      <ExpandableCard.Item
        titleIcon={require("../assets/svg/icon_ame.svg")}
        title="Titulo do Card"
        truncate={2}
        description={
          "Suspendisse in nibh at metus lobortis imperdiet id sit amet velit. Proin tincidunt felis id sapien lacinia sagittis. Maecenas in dui ornare elit mollis tincidunt ac ut elit."
        }
      >
        <Paragraph size="xs">[b]Identificador:[/b] Conteúdo filho</Paragraph>
      </ExpandableCard.Item>

      <ExpandableCard.Item
        titleIcon={require("../assets/svg/icon_ame.svg")}
        title="Titulo do Card"
        borderColor="amecolor-primary-medium"
        truncate={1}
        description={"Suspendisse in nibh."}
      >
        <Paragraph size="xs">[b]Identificador:[/b] Conteúdo filho</Paragraph>
      </ExpandableCard.Item>
    </ExpandableCard>

    <Spacing />

    <ExpandableCard
      type="selection"
      radioName="seletor"
      resetSelect={this.state.reset}
      onChange={(value) => console.log(value)}
    >
      <ExpandableCard.Item
        title="Titulo 1"
        truncate={1}
        description={"Conteudo 1"}
        radioValue="Valor 1"
      >
        <Paragraph size="xs">[b]Identificador:[/b] Conteúdo filho 1</Paragraph>
      </ExpandableCard.Item>

      <ExpandableCard.Item
        title="Titulo 2"
        truncate={1}
        description={"Conteudo 2"}
        radioValue="Valor 2"
      >
        <Paragraph size="xs">[b]Identificador:[/b] Conteúdo filho 2</Paragraph>
      </ExpandableCard.Item>

      <ExpandableCard.Item
        title="Titulo 3"
        truncate={1}
        description={"Conteudo 3"}
        radioValue="Valor 3"
      >
        <Paragraph size="xs">[b]Identificador:[/b] Conteúdo filho 3</Paragraph>
      </ExpandableCard.Item>
    </ExpandableCard>
  </View>
  <View>
    <Button
      label="Reset"
      type="primary"
      onClick={() => {
        this.setState({ reset: !this.state.reset });
      }}
    />
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/expandablecard/"
        );
      }}
    />
  </View>
</View>;
