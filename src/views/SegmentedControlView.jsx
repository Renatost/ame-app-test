<Window>
  <View>
    <View paddingHorizontal={20}>
      <Header fontSize={24} marginBottom={0}>
        Mini-app Debugger
      </Header>
      <Header fontSize={22}>SegmentedControl</Header>
      <Paragraph fontSize={18}>
        O SegmentedControl é o nosso componente de abas de navegação.
      </Paragraph>
      <Paragraph>Toque nos títulos para trocar a visualização</Paragraph>
      <Paragraph>
        Suas abas podem ser exibidas em duas formas: na parte superior ou
        inferior.
      </Paragraph>
      <Spacing />
      <SegmentedControl
      defaultActiveTab={0}
        onActiveTab={(i) => console.log(i)}
        items={[
          {
            title: "Dogs",
            content: <ListView items={dogs} />,
          },
          {
            title: "Cats",
            content: <ListView items={cats} />,
          },
        ]}
      />
    </View>
    <View background="pluscolor-secondary-darkest">
      <SegmentedControl onActiveTab={(i) => this.teste(i)} defaultActiveTab={1} >
        <SegmentedControl.Item title="Moto G8 Plus">
          <View>
            <Paragraph color="neutralcolor-light">Moto G8 Plus</Paragraph>

            <Card
              dataCy="card-grid"
              title={"Moto G8 Plus"}
              onClick={() => console.log("Moto G8 Plus")}
            />
          </View>
        </SegmentedControl.Item>
        <SegmentedControl.Item title="Samsung Galaxy A50">
          <View>
            <Paragraph color="neutralcolor-light">Samsung Galaxy A50</Paragraph>

            <Card
              dataCy="card-grid"
              title={"Samsung Galaxy A50Plus"}
              onClick={() => console.log("Samsung Galaxy A50")}
            />
          </View>
        </SegmentedControl.Item>
        <SegmentedControl.Item title="Xiaomi M1">
          <View>
            <Paragraph color="neutralcolor-light">Xiaomi M1</Paragraph>

            <Card
              dataCy="card-grid"
              title={"Xiaomi M1"}
              onClick={() => console.log("Xiaomi M1")}
            />
          </View>
        </SegmentedControl.Item>
      </SegmentedControl>
    </View>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/segmentedControl/"
        );
      }}
    />
  </View>
</Window>;
