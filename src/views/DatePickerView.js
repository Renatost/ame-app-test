import Ame from 'ame-super-app-client'
import { StyleResolver } from 'ame-miniapp-components'

export default class DatePickerView {

    state = {
        date: new Date(),
        minDate: new Date(),
        maxDate: new Date()
    }


    onDateChange = ( value ) => {
        console.log( 'new date', value )
        this.setState( {
            date: value
        } )
    }

    componentDidMount () {
        let maxDate = this.state.maxDate
        this.setState( { maxDate: maxDate.setDate( maxDate.getDate() + 10 ) } )
    }
}
