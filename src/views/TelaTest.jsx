<Window>
  <View height="100vh">
    <View
      height="90vh"
      background="pluscolor-primary-lightest"
      borderRadius="sm"
    >
      <ScrollView scrollTo={this.state.chat.length - 1}>
        {this.state.chat.length > 0 &&
          this.state.chat.map((msg, index) => {
            return (
              <View marginY="micro" key={index}>
                <Paragraph fontSize="sm" textAlign="right">
                  {msg}
                </Paragraph>
              </View>
            );
          })}
      </ScrollView>
    </View>
    <View>
      <Input
        value={this.state.inputValue}
        onChange={(msg) => this.handleText(msg)}
        onSubmit={this.newMessage}
      />
    </View>
  </View>
</Window>;
