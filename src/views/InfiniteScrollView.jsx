<Window>
  <View>
    <Header>InfiniteScroll</Header>
    <Paragraph>Componente usado para listar elementos na tela.</Paragraph>
  </View>
  <InfiniteScroll
    height="400px"
    initialItems={this.state.pokemons}
    onScrollEnd={this.getNextPokemons}
    renderItem={this.generateItem}
    endingFactor={0}
    showLoading={
      <Animator animationData={require("../assets/svg/dolphin.json")} />
    }
  />
  {/* <Spacing size="md" />
  <Paragraph>Componente desabilitado.</Paragraph>
  <Spacing size="md" />
  <View justify="center" padding="none" paddingX="md" height={400}>
    <InfiniteScroll
      height={"400px"}
      disabled
      dataCy="infiniteScroll-disabled"
      initialItems={this.state.pokemons}
      onScrollEnd={this.addItems}
      renderItem={this.generateItemDisabled}
      endingFactor={0}
      showLoading={
        <Animator animationData={require("../assets/svg/dolphin.json")} />
      }
    />
  </View> */}
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/infiniteScroll/"
        );
      }}
    />
  </View>
  <Card />
</Window>;
