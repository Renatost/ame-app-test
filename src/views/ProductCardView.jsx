<Window>
<Group>
        <View>
          <Header size='large'>Lista de Products Card</Header>
          <Spacing />
          {this.state.products
            .reduce((acu, curr, ind) => {
              ind % 2 ? acu[acu.length - 1].push(curr) : acu.push([curr]);
              return acu;
            }, [])
            .map((item, i) => (
              <Grid key={`grid_${i}`}>
                {item.map((product, productIndex) => (
                  <Group key={`product_${productIndex}`}>
                    <ProductCard
                      height={270}
                      cardOrientation={'vertical'}
                      onClick={() => console.log('ProductCard')}
                      productImage={
                        product.images[0].extraLarge ||
                        product.images[0].large ||
                        product.images[0].big ||
                        product.images[0].medium
                      }
                      productName={product.name
                        .split(' ')
                        .slice(0, 3)
                        .join(' ')}
                      conditionPayment={'5% cashback'}
                      price={15000}
                    />
                    <Spacing />
                  </Group>
                ))}
              </Grid>
            ))}
        </View>
      </Group>
      <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/productCard/')
            }}
        />
    </View>
</Window>