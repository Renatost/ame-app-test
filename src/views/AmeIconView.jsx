<Window>
    <View>
        <Header>Componente AmeIcon</Header>
        <Paragraph>Esse componente serve para você colocar icones no seu miniapp!</Paragraph>
    </View>
    <View>
        <Grid>
            <AmeIcon icon={'ame-outline'} color="red"/>
            <AmeIcon icon={'ame-solid'} />
            <AmeIcon icon={'home-outline'} />
            <AmeIcon icon={'home-solid'} />
        </Grid>
        <Grid>
            <AmeIcon icon={'services-solid'} onClick={() => console.log('icon')} />
            <AmeIcon icon={'services-outline'} />
            <AmeIcon icon={'profile-solid'} />
            <AmeIcon icon={'profile-outline'} />
        </Grid>
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/ameIcon/')
            }}
        />
    </View>
</Window>