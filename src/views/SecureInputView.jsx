<Window>
  <Header>SecureInput</Header>
  <Spacing />
    <Group>
      <Subtitle>Este é um SecureInput de número: </Subtitle>
      <View>
          <SecureInput
            inputMode='numeric'
            length={4}
            onChange={e => console.log(e)}
            color={'pluscolor-secondary-light'}
            showCaracters
          />
      </View>
    </Group>
    <Spacing />
    <Group>
      <Subtitle>Este é um SecureInput de texto: </Subtitle>
      <View background="amegradient-secondary">
          <SecureInput
            inputMode='text'
            length={6}
            onChange={e => console.log(e)}
            color={'pluscolor-primary-light'}
          />
      </View>
    </Group>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/secureInput/')
            }}
        />
    </View>
</Window>