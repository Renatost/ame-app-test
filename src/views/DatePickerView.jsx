<Window>
  <View>
    <View paddingHorizontal={20}>
      <Header fontSize={24} marginBottom={0}>
        Mini-app Debugger
      </Header>
      <Header fontSize={22}>DatePicker</Header>
      <Spacing />
      <Paragraph fontSize={18}>Hora de testar o datepicker.</Paragraph>
      <Spacing />
      <DatePicker
        disabled={false}
        minDate={this.state.minDate}
        maxDate={this.state.maxDate}
        onChange={this.onDateChange}
        value={this.state.date}
        format={"DD/MM/YYYY"}
      />
      <Divisor />
    </View>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/datePicker/"
        );
      }}
    />
  </View>
</Window>;
