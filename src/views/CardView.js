export default class CarView {

    state = {
        showCreditCard: true
    }

    onChangeHandler = e => {
        this.setState({ showCreditCard: !this.state.showCreditCard }, () => {
            console.log(
                e
                    ? 'OptIn ON (valor retornado: ' + e + ')'
                    : 'OptIn OFF (valor retornado: ' + e + ')'
            )
        })
    }

}
