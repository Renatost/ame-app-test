<Window>
  <View>
    <View paddingX="sm">
      <Header>CurrencyInput</Header>
      <Paragraph>
        Componente usado para a formatação de moeda em tempo real.{" "}
      </Paragraph>
      <Paragraph>Componente de texto editável</Paragraph>

      <Divisor />

      <View>
        <CurrencyInput
          maxLength={8}
          prefix="€"
          value={this.state.value}
          onChange={(value) => {
            console.log(value);
          }}
        />
      </View>
      <View>
        <Paragraph>Valor alterando vindo de uma api/localStorage</Paragraph>
        <Spacing size="md" />
        <CurrencyInput
          maxLength={8}
          prefix="€"
          value={this.state.localStorageValue}
          onChange={(value) => {
            console.log(value);
          }}
        />
      </View>
      <View>
        <Paragraph>Valor alterando vindo de uma api/localStorage</Paragraph>
        <Spacing size="md" />
        <CurrencyInput
          maxLength={8}
          flat={true}
          prefix="€"
          value={this.state.localStorageValue}
          onChange={(value) => {
            console.log(value);
          }}
        />
      </View>
    </View>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/currencyInput/"
        );
      }}
    />
  </View>
</Window>;
