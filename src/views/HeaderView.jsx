<Window>
    <View>
        <Header>Cabeçalhos</Header>
        <Spacing size='sm' />
        <Paragraph>
        Componentes de apresentação primária dos assuntos. Vamos começar
        pelos componentes Header, que podem ter vários tamanhos.
        </Paragraph>
        <Spacing size='sm' />
        <Paragraph color='amecolor-primary-medium'>
        Cabeçalho tamanho display.
        </Paragraph>
        <Header size='display'>The Fox say</Header>
        <Spacing size='sm' />
        <Paragraph color='amecolor-primary-medium'>
        Cabeçalho tamanho XL.
        </Paragraph>
        <Header size='xl'>What the fox</Header>
        <Spacing size='sm' />
        <Paragraph color='amecolor-primary-medium'>
        Cabeçalho tamanho Large.
        </Paragraph>
        <Header size='large'>What does the fox</Header>
        <Spacing size='sm' />
        <Paragraph color='amecolor-primary-medium'>
        Cabeçalho tamanho Default.
        </Paragraph>
        <Header size='default'>What does the fox say</Header>
        <Spacing size='sm' />
        <Paragraph color='amecolor-primary-medium'>
        Cabeçalho tamanho SM
        </Paragraph>
        <Header size='default'>What does the fox say !!!!</Header>
        <Spacing size='sm' />
        <Paragraph color='amecolor-primary-medium'>
        Cabeçalho tamanho XS.
        </Paragraph>
        <Header size='xs'>What does the fox say !!!!!</Header>
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/header/')
            }}
        />
    </View>
</Window>