<Window>
  <View>
    <View width={"100vw"} height={"80vh"}>
      <Animator animationData={this.ANIMATION} />
    </View>
    <View width={"100vw"} height={"80vh"}>
      <Animator animationData={require('../assets/svg/dolphin.json')} />
    </View>
    <View>
      <Spacing />
      <TextLink
        text="Link para documentação"
        onClick={() => {
          window.open(
            "https://ame-miniapp-components.calindra.com.br/docs/animator/"
          );
        }}
      />
    </View>
  </View>
</Window>;
