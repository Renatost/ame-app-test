<Group>
    <View>
        <Header>Grid</Header>
        <Spacing />
        <Paragraph>
        Ao inserir componentes dentro do grid ele divide igualmente os
        espaços
        </Paragraph>
        <Spacing size='sm' />
        <Paragraph>Inserimos abaixo, quatro botões circulares</Paragraph>
        <Spacing />
        <Grid columns={2}>
        {[
            <Circle
            key={1}
            icon={require('../assets/svg/icon_ame.svg')}
            />,
            <Circle
            key={2}
            icon={require('../assets/svg/icon_ame.svg')}
            />,
            <Circle
            key={3}
            icon={require('../assets/svg/icon_ame.svg')}
            />,
            <Circle
            key={4}
            icon={require('../assets/svg/icon_ame.svg')}
            />,
        ]}
        </Grid>
        <Spacing size='sm' />
        <Paragraph>Inserimos abaixo, três imagens</Paragraph>
        <Spacing />
        <Grid>
        {[
            <Banner key={1} images={['https://placekitten.com/300/300']} />,
            <Banner key={2} images={['https://placekitten.com/300/300']} />,
            <Banner key={3} images={['https://placekitten.com/300/300']} />,
        ]}
        </Grid>
        <Spacing size='sm' />
        <Paragraph>Inserimos abaixo, dois FlatCards</Paragraph>
        <Spacing />
        <Grid columns={1}>
        {[
            <FlatCard
            key={1}
            cardType={'details'}
            price={1000}
            plots={'em até 12x parcelas'}
            paymentshape={'Ame'}
            />,
            <FlatCard
            key={2}
            cardType={'details'}
            price={1000}
            plots={'em até 12x parcelas'}
            paymentshape={'Ame'}
            />,
        ]}
        </Grid>
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/grid/')
            }}
        />
    </View>
</Group>