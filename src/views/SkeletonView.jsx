<Window>
    <Header>Skeleton</Header>
    <Paragraph block marginBottom={20}>
        Os dados dos seus componentes podem não estar imediatamente
        disponíveis. Você pode aumentar o desempenho percebido pelos usuários
        usando esqueletos. Ele passa a sensação de que as coisas estão
        acontecendo imediatamente.
    </Paragraph>
    <Paragraph block marginBottom={4}>
        Este é um Skeleton padrão.
    </Paragraph>
    <Skeleton marginBottom={10} />
    <Paragraph block marginBottom={10}>
        Você pode usar três Skeleton para simular um texto.
    </Paragraph>
    <Skeleton variant={'text'} marginBottom={10} />
    <Skeleton variant={'text'} marginBottom={10} />
    <Skeleton variant={'text'} marginBottom={20} />
    <Paragraph block marginBottom={4}>
        Este é um Skeleton com sua variação circle.
    </Paragraph>
    <Skeleton variant={'circle'} size={120} />
    <Paragraph block marginBottom={4}>
        Este é um Skeleton com sua variação rect.
    </Paragraph>
    <Skeleton variant={'rect'} />
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/skeleton/')
            }}
        />
    </View>
</Window>