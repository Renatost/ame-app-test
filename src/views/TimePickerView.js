export default class TimePickerTest {
    constructor(props) {
        super(props);
        this.state = {
          time: '12:00',
          minTime: '10:00',
          maxTime: '15:00'
        };
      }
    
      changeTime = value => {
        this.setState({ time: value });
      };
    
      changeTimeTo14 = () => {
        this.setState({ time: '14:00' });
      };
}