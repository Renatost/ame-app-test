export default class TouchableView {
  handleClick = () => {
    console.log("Ação de click");
  };

  disabledClick = () => {
    console.log("Ação de click não executada por estar desabilitado");
  };
}
