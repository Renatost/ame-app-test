export default class TelaTest extends React.Component {
  state = {
    chat: [],
    inputValue: "",
  };

  handleText = (msg) => {
    this.setState({ inputValue: msg });
  };

  newMessage = () => {
    const msg = this.state.inputValue;
    let newChat = this.state.chat;
    newChat.push(msg);
    this.setState({ chat: newChat });
    this.setState({ inputValue: "" });
    console.log(this.state.chat);
    console.log(this.state.chat.length);
  };
}
