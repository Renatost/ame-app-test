export default class InputTest {
  state = {
    inputPadrao: "",
    loading: true,
    to: "NONE",
    date: new Date(),
    address: {},
    user: { firstName: "", lastName: "" },
    optionList: [
      {
        value: "FLA",
        label: "Flamengo",
        fullName: "Clube de Regatas do Flamengo",
        foundation: "15 de novembro de 1895",
      },
      {
        value: "BOT",
        label: "Botafogo",
        fullName: "Botafogo de Futebol e Regatas",
        foundation: "1 de julho de 1894",
      },
      {
        value: "FLU",
        label: "Fluminense",
        fullName: "Fluminense Football Club",
        foundation: "21 de julho de 1902 ",
      },
      {
        value: "VAS",
        label: "Vasco da Gama",
        fullName: "Club de Regatas Vasco da Gama",
        foundation: "21 de agosto de 1898",
      },
    ],
  };

  componentDidMount = () => {
    setTimeout(() => {
      this.setState({
        inputPadrao: "value carregado.",
      });
    }, 1000);
  };

  inputSearch = (e) => {
    console.log(e);
  };

  handleSubmit = () => {
    console.log(this.state.user)
  }
}
