<Window>
  <View paddingHorizontal={20}>
    <Header fontSize="large" marginBottom={0}>
      Mini-app Debugger
    </Header>
    <Header fontSize="md">ListView</Header>
    <Spacing />
    <Paragraph fontSize="sm">O ListView exibe uma lista de itens.</Paragraph>
    <Spacing />
    <ListView
      items={cursos}
      rightIcon="right-next"
      rightIconColor='red'
      onItemSelect={onClick}
      rightIconClick={(e) => console.log(e)}
    />
    <Spacing />
    <Paragraph fontSize="xs">ListView com icones na esquerda.</Paragraph>
    <Spacing />
    <ListView
      items={pagamentos}
      rightIcon={require("../assets/svg/Seta.svg")}
      rightIconClick={onClick}
      onItemSelect={(e) => console.log(e)}
    />
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/listView/"
        );
      }}
    />
  </View>
</Window>;
