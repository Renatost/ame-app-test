<Window>
    <View>
        <View paddingHorizontal={20}>
            <Header fontSize={24} marginBottom={0}>Mini-app Debugger</Header>
            <Header fontSize={22}>Header</Header>
            <Paragraph fontSize={18}>O Header é o nosso componente de título.</Paragraph>
            <Header>Este é um Header</Header>
            <Header
                size={1}
                fontSize={20}
                fontWeight={"bold"}
                fontFamily={"serif"}
                lineHeight={32}
                color={"#db1a64"}
            >Este é um Header com customizações</Header>
        </View>
    </View>
</Window>
