<View>
    <Header size='display'>Modal</Header>
    <Spacing />
    <Subtitle>Modal para exibir conteúdo.</Subtitle>
    <Spacing />
    <Paragraph>
        Clique no botão abaixo para abrir o modal centralizado
    </Paragraph>
    <Spacing />
    <TextLink onClick={() => this.openModal('center')} text='Abrir' />
    <Spacing />

    <Paragraph>
        Clique no botão abaixo para abrir o modal no bottom
    </Paragraph>
    <Spacing />
    <TextLink onClick={() => this.openModal('bottom')} text='Abrir' />
    <Spacing />

    <Modal
        show={this.state.showModal}
        position={this.state.position}
        closeModalCallback={this.closeModal}>
        <Illustration
        height={200}
        image={require('../assets/pictures/ilustra-1.png')}
        />
        <Spacing />
        <Paragraph>Lorem ipsum dolor sit amet</Paragraph>
        <Spacing size='md' />
        <Button
        label='Continuar'
        type='primary'
        onClick={() => console.log('Continuar')}
        />
        <Spacing />
        <Button
        label='Cancelar'
        type='secondary'
        onClick={() => this.closeModal()}
        />
    </Modal>
    <View>
    <Spacing />
    <TextLink
        text='Link para documentação'
        onClick={() => {
        window.open('https://ame-miniapp-components.calindra.com.br/docs/modal/')
        }}
    />
    </View>
</View>