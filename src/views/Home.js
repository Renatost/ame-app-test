import Ame from "ame-super-app-client";

export default class Home {
  state = {
    hasBottomGestureBar: false,

    // Estes são os itens utilizados pelo listView dentro do arquivo Home.jsx
    screens: [
      {
        key: "1",
        title: "Tela ScrollView",
        subtitle: "Tela para teste do ScrollView",
        to: "TelaTest",
      },
    ],
    simple: [
      {
        key: "1",
        title: "Window",
        subtitle: "Componente mestre que precisa envolver toda tela.",
        to: "WindowView",
      },
      {
        key: "2",
        title: "Group",
        subtitle: "Usado para organizar conteúdo.",
        to: "GroupView",
      },
      {
        key: "3",
        title: "View",
        subtitle: "Menor unidade de divisão de conteúdo.",
        to: "ViewView",
      },
      {
        key: "4",
        title: "Grid",
        subtitle: "Usado para organizar componentes.",
        to: "GridView",
      },
      {
        key: "5",
        title: "Touchable",
        subtitle: "Usado para dar acção de click a componentes.",
        to: "TouchableView",
      },
    ],

    texts: [
      {
        key: "1",
        title: "Paragraph",
        subtitle: "Exibe um parágrafo na tela.",
        to: "ParagraphView",
      },
      {
        key: "2",
        title: "Header",
        subtitle: "Exibe um cabeçalho na tela.",
        to: "HeaderView",
      },
      {
        key: "3",
        title: "Subtitle",
        subtitle: "Exibe um subtítulo na tela.",
        to: "SubtitleView",
      },
      {
        key: "4",
        title: "Bullet",
        subtitle: "Desenha um item para listagem.",
        to: "BulletView",
      },
      {
        key: "5",
        title: "Hint",
        subtitle: "Componente para apresentar alertas.",
        to: "HintView",
      },
      {
        key: "6",
        title: "Text",
        subtitle: "Componente para textos em geral.",
        to: "TextView",
      },
    ],
    illustrations: [
      {
        key: "1",
        title: "Banner",
        subtitle:
          "Utilizado para a exibição de imagens, podendo ter uma ação de toque.",
        to: "BannerView",
      },
      {
        key: "2",
        title: "Illustration",
        subtitle: "Exibe uma imagem centralizada em sua caixa.",
        to: "IllustrationView",
      },
      {
        key: "3",
        title: "Animator",
        subtitle: "Componente para exibir animações.",
        to: "AnimatorView",
      },
      {
        key: "4",
        title: "AmeIcon",
        subtitle: "Componente que exibe um Ícone.",
        to: "AmeIconView",
      },
      {
        key: "5",
        title: "Video",
        subtitle: "Componente para renderização de vídeo.",
        to: "VideoView",
      },
      {
        key: "6",
        title: "Image",
        subtitle: "Componente para renderização de imagens e icons.",
        to: "ImageView",
      },
    ],
    interactions: [
      {
        key: "1",
        title: "Button",
        subtitle: "Exibe um botão na tela.",
        to: "ButtonView",
      },
      {
        key: "2",
        title: "Dropdown",
        subtitle: "Exibe na tela um componente para esolha de opções.",
        to: "DropdownView",
      },
      {
        key: "3",
        title: "SegmentedControl",
        subtitle: "Navegação em abas.",
        to: "SegmentedControlView",
      },
      {
        key: "4",
        title: "Input",
        subtitle: "Insere um campo para inserção de dados.",
        to: "InputView",
      },
      {
        key: "5",
        title: "CurrencyInput",
        subtitle: "Exibe uma entrada de valor monetário na tela.",
        to: "CurrencyInputView",
      },
      {
        key: "6",
        title: "OptIn",
        subtitle: "Componente para aceitar termos.",
        to: "OptInView",
      },
      {
        key: "7",
        title: "DatePicker",
        subtitle: "Componente para selecionar data.",
        to: "DatePickerView",
      },
      {
        key: "8",
        title: "TimePicker",
        subtitle: "Componente para selecionar hora.",
        to: "TimePickerView",
      },
      {
        key: "9",
        title: "CheckItem",
        subtitle: "Caixa de seleção.",
        to: "CheckItemView",
      },
      {
        key: "10",
        title: "Information",
        subtitle: "Exibir uma mensagem de alerta.",
        to: "InformationView",
      },
      {
        key: "11",
        title: "SecureInput",
        subtitle: "Exibe uma entrada de valor de forma segura.",
        to: "SecureInputView",
      },
      {
        key: "12",
        title: "PullToRefresh",
        subtitle: "Componente para atualização da página ao realizar um pull.",
        to: "PullToRefreshView",
      },
      {
        key: "13",
        title: "Map",
        subtitle: "Componente utilizado para a exibir a localização no mapa.",
        to: "MapView",
      },
      {
        key: "14",
        title: "Ads",
        subtitle: "Componente Ads para renderização de anuncios.",
        to: "AdsView",
      },
      {
        key: "15",
        title: "DestionationMap",
        subtitle: "Componente Destination",
        to: "DestinationMapView",
      },
      {
        key: "16",
        title: "Modal",
        subtitle: "Componente Modal",
        to: "ModalView",
      },

      {
        key: "17",
        title: "Slider",
        subtitle: "Componente Slider",
        to: "SliderView",
      },
      {
        key: "18",
        title: "Invoice Input",
        subtitle: "Componente para máscara de códigos de barra entre outros.",
        to: "InvoiceInputView",
      },
      {
        key: "19",
        title: "Radio",
        subtitle: "Componente utilizado para seleção entre opções.",
        to: "RadioView",
      },
    ],
    organization: [
      {
        key: "1",
        title: "Divisor",
        subtitle: "Componente pequeno para separar conteúdos.",
        to: "DivisorView",
      },
      {
        key: "2",
        title: "Skeleton",
        subtitle: "Usado para diminuir ou incrementar um valor.",
        to: "SkeletonView",
      },
      {
        key: "3",
        title: "SpringLayout",
        subtitle: "Componente com formatação de tela padrão.",
        to: "SpringLayoutView",
      },
      {
        key: "4",
        title: "Carousel",
        subtitle: "Exibe conteúdos dentro de um carrossel.",
        to: "CarouselView",
      },
      {
        key: "5",
        title: "Nav",
        subtitle: "Componente barra de navegação.",
        to: "NavView",
      },
      {
        key: "6",
        title: "Accordion",
        subtitle: "Componente Accordion.",
        to: "AccordionView",
      },
      {
        key: "7",
        title: "Datasheet",
        subtitle: "Componente Datasheet.",
        to: "DatasheetView",
      },
      {
        key: "8",
        title: "BulletAnimation",
        subtitle: "Componente BulletAnimation.",
        to: "BulletAnimationView",
      },
      {
        key: "9",
        title: "Steps",
        subtitle: "Componente Steps.",
        to: "StepsView",
      },
      {
        key: "10",
        title: "BoxShadow",
        subtitle: "Componente BoxShadow.",
        to: "BoxShadowView",
      },
    ],
    lists: [
      {
        key: "1",
        title: "ListView",
        subtitle: "Lista útil para ser usada como um navegador.",
        to: "ListViewView",
      },
      {
        key: "2",
        title: "InfiniteScroll",
        subtitle: "Semelhante ao Scroll Infinito do Twitter.",
        to: "InfiniteScrollView",
      },
      {
        key: "3",
        title: "DetailList",
        subtitle: "Componente que gera uma lista de detalhes com icones.",
        to: "DetailListView",
      },
      {
        key: "4",
        title: "ScrollView",
        subtitle: "Componente que gera uma lista de detalhes com icones.",
        to: "ScrollViewView",
      },
    ],
    cards: [
      {
        key: "1",
        title: "Card",
        subtitle: "Componente que contêm um conteúdo e ações.",
        to: "CardView",
      },
      {
        key: "2",
        title: "ProductCard",
        subtitle: "Utilizado para a exposição de produtos.",
        to: "ProductCardView",
      },
      {
        key: "3",
        title: "FlatCard",
        subtitle: "Utilizado para a descrição de preços dos produtos.",
        to: "FlatCardView",
      },
      {
        key: "4",
        title: "ImageFlatCard",
        subtitle: "Componente ImageFlatCard",
        to: "ImageFlatCardView",
      },
      {
        key: "5",
        title: "Circle",
        subtitle: "Botão em formato circular.",
        to: "CircleView",
      },
      {
        key: "6",
        title: "ExpandableCard",
        subtitle: "Card expansivel.",
        to: "ExpandableCardView",
      },
      {
        key: "7",
        title: "SelectCard",
        subtitle: "Card expansivel.",
        to: "SelectCardView",
      },
    ],
  };

  navigateTo = (item) => {
    Ame.navigation.navigate(item.to);
  };

  async componentDidMount() {
    const view = this.COMPONENT_LIST.filter((x) => x.key === 22)[0]["view"];
    console.log(`Navegando para: ${view}`);
    const { hasBottomGestureBar } = await Ame.getDeviceSpecs();
    this.setState({ hasBottomGestureBar });
  }

  updateList = () => {
    return new Promise((resolve) => {
      setTimeout(resolve, 5000);
    });
  };
}
