<Window>
  <View>
    <Header>Parágrafos</Header>
    <Spacing size="sm" />
    <Paragraph size="xs" textAlign="center">
      Outros textos explicativos deverão ser aplicados em parágrafos. Este
      Paragraph possui size='xs' e textAlign='center'
    </Paragraph>
    <Spacing size="sm" />
    <Paragraph color="amecolor-primary-medium">
      Parágrafo tamanho BBCODE.
    </Paragraph>
    <Paragraph size="lg">This [u]text[/u] is [s]awesome[/s]!!</Paragraph>
    <Spacing size="sm" />
    <Paragraph color="amecolor-primary-medium">
      Parágrafo tamanho Default.
    </Paragraph>
    <Paragraph size="default">This text is awesome!!</Paragraph>
    <Spacing size="sm" />
    <Paragraph color="amecolor-primary-medium">Parágrafo tamanho MD.</Paragraph>
    <Paragraph size="md">This text is awesome!!</Paragraph>
    <Spacing size="sm" />
    <Paragraph color="amecolor-primary-medium">Parágrafo tamanho SM.</Paragraph>
    <Paragraph size="sm">This text is awesome!!</Paragraph>
    <Spacing size="sm" />
    <Paragraph color="amecolor-primary-medium">Parágrafo tamanho XS.</Paragraph>
    <Paragraph size="xs">This text is awesome!!</Paragraph>
    <Spacing size="sm" />
      <Text
        fontSize="xxs"
        fontWeight="bold"
        truncate="2"
        color="amecolor-primary-medium"
      >
        Novo componente de
        <Text selectable>[u][color='amecolor-primary-dark']Texto[/color][/u]</Text>
        para uso nos miniapps da AME.
      </Text>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/paragraph/"
        );
      }}
    />
  </View>
</Window>;
