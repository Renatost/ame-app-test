<Window>
        <Nav
          backgroundColor={settings.ColorNavbar}
          iconColor={settings.ColorNavbarButtons}
          sticky
          safeArea={true}
          onBackClick={this.goToBack}
          onCloseClick={this.goToClose}
        />
        <Spacing />
        <View
          background={settings.ColorScreensBackground}
          direction={'column'}
          align={'center'}>
          <Ads
            path='/22158792083/miniapp_bf/rotativo1_1'
            size={['fluid', [335, 110]]}
            id={'div-gpt-ad-1607094047807-0'}
          />
          {/*     <Carousel infinite={false} autoplay={false} slidesToShow={2} slidesToScroll={2}>
                  <Ads path='/22158792083/miniapp_bf/rotativo1_1' size={['fluid', [335, 110]]} id={'div-gpt-ad-1607094047807-0'}/>
                  <Ads path='/22158792083/miniapp_bf/rotativo1_2' size={['fluid', [335, 110]]} id={'div-gpt-ad-1607094131039-0'}/>
              </Carousel> */}
          <Spacing size={'sm'} />
          <Block width='335px' height='auto'>
            <Paragraph fontWeight='bold' color={settings.ColorParagraph}>
              Tudo no seu super app
            </Paragraph>
            <Spacing />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/circulo_1'
                size={['fluid', [76, 110]]}
                id={'div-gpt-ad-1607095547740-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/circulo_2'
                size={['fluid', [76, 110]]}
                id={'div-gpt-ad-1607095554341-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/circulo_3'
                size={['fluid', [76, 110]]}
                id={'div-gpt-ad-1607095562427-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/circulo_4'
                size={['fluid', [76, 110]]}
                id={'div-gpt-ad-1607095571948-0'}
              />
            </Grid>
            <Spacing size={'sm'} />
            <Paragraph fontWeight='bold' color={settings.ColorParagraph}>
              Melhores Cashbacks
            </Paragraph>
            <Spacing />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/quadrado1_1'
                size={['fluid', [162, 162]]}
                id={'div-gpt-ad-1607094642919-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/quadrado1_2'
                size={['fluid', [162, 162]]}
                id={'div-gpt-ad-1607094645422-0'}
              />
            </Grid>
            <Spacing size={'xxxs'} />
            <Ads
              path='/22158792083/miniapp_bf/retangulo_1'
              size={['fluid', [335, 110]]}
              id={'div-gpt-ad-1607094648839-0'}
            />
            <Spacing size={'xxxs'} />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/quadrado2_1'
                size={['fluid', [162, 162]]}
                id={'div-gpt-ad-1607094763373-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/quadrado2_2'
                size={['fluid', [162, 162]]}
                id={'div-gpt-ad-1607094778985-0'}
              />
            </Grid>
            <Spacing size={'sm'} />
            <Paragraph fontWeight='bold' color={settings.ColorParagraph}>
              Tudo o que você precisa
            </Paragraph>
            <Spacing />
            <Ads
              path='/22158792083/miniapp_bf/retangulo_2'
              size={['fluid', [335, 110], [400, 131]]}
              id={'div-gpt-ad-1607094792672-0'}
            />
            <Spacing size={'xxxs'} />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/quadrado3_1'
                size={['fluid', [162, 162]]}
                id={'div-gpt-ad-1607094830073-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/quadrado3_2'
                size={['fluid', [162, 162]]}
                id={'div-gpt-ad-1607094841822-0'}
              />
            </Grid>
            <Spacing size={'sm'} />
            <Paragraph fontWeight='bold' color={settings.ColorParagraph}>
              Marcas que todo mundo ama
            </Paragraph>
            <Spacing />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/marca_1'
                size={['fluid', [76, 53]]}
                id={'div-gpt-ad-1607094913553-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/marca_2'
                size={['fluid', [76, 53]]}
                id={'div-gpt-ad-1607094921870-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/marca_3'
                size={['fluid', [76, 53]]}
                id={'div-gpt-ad-1607094930882-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/marca_4'
                size={['fluid', [76, 53]]}
                id={'div-gpt-ad-1607094937818-0'}
              />
            </Grid>
            <Spacing size={'sm'} />
            <Paragraph fontWeight='bold' color={settings.ColorParagraph}>
              Favoritos da Galera
            </Paragraph>
            <Spacing />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/miniret_1'
                size={['fluid', [162, 100]]}
                id={'div-gpt-ad-1607095140694-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/miniret_2'
                size={['fluid', [162, 100]]}
                id={'div-gpt-ad-1607095144324-0'}
              />
            </Grid>
            <Spacing size={'xxxs'} />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/miniret_3'
                size={['fluid', [162, 100]]}
                id={'div-gpt-ad-1607095145782-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/miniret_4'
                size={['fluid', [162, 100]]}
                id={'div-gpt-ad-1607095147467-0'}
              />
            </Grid>
            <Spacing size={'sm'} />
            <Paragraph fontWeight='bold' color={settings.ColorParagraph}>
              Lojas Físicas
            </Paragraph>
            <Spacing />
            <Ads
              path='/22158792083/miniapp_bf/retangulo_3'
              size={['fluid', [335, 110]]}
              id={'div-gpt-ad-1607095328582-0'}
            />
            <Spacing size={'xxxs'} />
            <Grid>
              <Ads
                path='/22158792083/miniapp_bf/miniret_5'
                size={['fluid', [162, 100]]}
                id={'div-gpt-ad-1607095324095-0'}
              />
              <Ads
                path='/22158792083/miniapp_bf/miniret_6'
                size={['fluid', [162, 100]]}
                id={'div-gpt-ad-1607095325316-0'}
              />
            </Grid>
            <Spacing size={'xxxs'} />
            <Ads
              path='/22158792083/miniapp_bf/retangulo_4'
              size={['fluid', [335, 110]]}
              id={'div-gpt-ad-1607095326938-0'}
            />
          </Block>
        </View>
        <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/ads/')
            }}
        />
    </View>
      </Window>