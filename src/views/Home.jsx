<Window pullToRefresh={this.updateList}>
  <View marginBottom={this.state.hasBottomGestureBar ? 40 : 20}>
    <View>
      <Image
        height={300}
        src={require("../assets/pictures/illustration.png")}
      />
      <Header textAlign="center">Miniapp ShowCase</Header>
      <Spacing size="xxs" />
      <Paragraph textAlign="center">
        Este mini-app será utilizado para que possamos visualizar todos os
        componentes da plataforma.
      </Paragraph>
      <Paragraph textAlign="center">
        ame-miniapp-components: [b]2.1.8[/b]
      </Paragraph>
      <Paragraph textAlign="center">
        ame-super-app-client: [b]2.2.0[/b]
      </Paragraph>
    </View>
    <View>
      <Paragraph>
        A lista de componentes abaixo está sendo renderizada através do
        componente ListView.
      </Paragraph>
    </View>
    <Spacing />
    <Divisor />
    <Spacing />
    <View>
      <Paragraph size={"lg"}>Telas de teste</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.screens}
        onItemSelect={this.navigateTo}
      />
    </View>
    <View>
      <Paragraph size={"lg"}>Componentes Simples</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.simple}
        onItemSelect={this.navigateTo}
      />
    </View>
    <Spacing size={"nano"} />
    <View>
      <Paragraph size={"lg"}>Textos</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.texts}
        onItemSelect={this.navigateTo}
      />
    </View>
    <Spacing size={"nano"} />
    <View>
      <Paragraph size={"lg"}>Ilustrações</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.illustrations}
        onItemSelect={this.navigateTo}
      />
    </View>
    <Spacing size={"nano"} />
    <View>
      <Paragraph size={"lg"}>Interações</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.interactions}
        onItemSelect={this.navigateTo}
      />
    </View>
    <Spacing size={"nano"} />
    <View>
      <Paragraph size={"lg"}>Organização</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.organization}
        onItemSelect={this.navigateTo}
      />
    </View>
    <Spacing size={"nano"} />
    <View>
      <Paragraph size={"lg"}>Listas</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.lists}
        onItemSelect={this.navigateTo}
      />
    </View>
    <Spacing size={"nano"} />
    <View>
      <Paragraph size={"lg"}>Cartões</Paragraph>
      <Spacing />
      <ListView
        showArrow
        items={this.state.cards}
        onItemSelect={this.navigateTo}
      />
    </View>
  </View>
</Window>;
