<Window>
  <View>
    <Header size="display">Carrossel</Header>
    <Spacing />
    <Subtitle>Carrossel com swipe para exibir imagens e conteúdo</Subtitle>
    <Spacing size="md" />
    <Paragraph>Carrossel com autoplay</Paragraph>
    <Spacing size="md" />
    <Carousel
      dataCy="carousel-auto"
      slidePerView={3}
      speed={2000}
      currentSlide={(e) => {
        console.log("currentSlide: ", e);
      }}
      lastSlide={(e) => {
        console.log("lastSlide: ", e);
      }}
      firstSlide={(e) => {
        console.log("firstSlide: ", e);
      }}
    >
      {[1, 2, 3, 4, 5].map((n) => this.renderCard(n))}
    </Carousel>
    <Spacing size="md" />
    <Paragraph>Carrossel sem o autoplay</Paragraph>
    <Spacing size="md" />
    <Carousel slideposition={this.state.initialSlide}>
      <Illustration
        image={require("../assets/pictures/illustration.png")}
        height={350}
      />
      <Illustration
        image={require("../assets/pictures/illustration.png")}
        height={350}
      />
      <Paragraph>Slide 2</Paragraph>
      <Paragraph>Slide 3</Paragraph>
    </Carousel>
    <Spacing />
    <Button dataCy="nextSlide" onClick={this.nextSlide} label="Próximo Slide" />
    <Spacing />
    <Button
      dataCy="firstSlide"
      onClick={this.firstSlide}
      label="Primeiro Slide"
    />
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/carousel/"
        );
      }}
    />
  </View>
  <Card />
</Window>;
