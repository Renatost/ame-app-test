<Window>
  <View>
    <View>
      <Header>Botões</Header>
      <Spacing />
      <View direction="row">
        <Paragraph textAlign="center">Desativado</Paragraph>
        <Toggle checked={!this.state.lock} onChange={this.handleOptIn} />
        <Paragraph textAlign="center">Ativado</Paragraph>
      </View>

      <Subtitle>Hora de testar os botões.</Subtitle>

      <View>
        <View></View>
        <Spacing stice="xs" />
        <View direction='column' align="center">
          <Paragraph>Primary</Paragraph>
          <Button
            onClick={() => this.handleButton("Primary")}
            disabled={this.state.lock}
            type="primary"
            label="Primario"
          />
        </View>
        <Spacing stice="xs" />
        <View>
          <Paragraph textAlign="center">Secondary</Paragraph>

          <Button
            onClick={() => this.handleButton("Secondary")}
            disabled={this.state.lock}
            type="secondary"
            label="Secundario"
          />
        </View>
        <Spacing stice="xs" />
        <View>
          <Paragraph textAlign="center">Tertiary</Paragraph>
          <Button
            onClick={() => this.handleButton("Tertiary")}
            disabled={this.state.lock}
            type="tertiary"
            label="Terciario"
            leftIcon='ame-outline'
            rightIcon='right-next'
          />
        </View>
        <Spacing stice="xs" />
        <View>
          <Paragraph textAlign="center">Quaternary</Paragraph>

          <Button
            onClick={() => this.handleButton("Quaternary")}
            disabled={this.state.lock}
            type="quaternary"
            label="Quaternario"
          />
        </View>
      </View>
      <Paragraph>
        Botão [b]{this.state.button}[/b] clicado {this.state.count} vezes!
      </Paragraph>
    </View>
  </View>
  <View>
    <Spacing />
    <TextLink
      text="Link para documentação"
      onClick={() => {
        window.open(
          "https://ame-miniapp-components.calindra.com.br/docs/button/"
        );
      }}
    />
  </View>
</Window>;
