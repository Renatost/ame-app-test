<Window>
    <View>
        <Header size='display'>Opt In</Header>
        <Spacing />
        <Paragraph>Um componente de aceite de termos de uso</Paragraph>
        <Spacing />
        <Banner image={'https://placekitten.com/600/300'} />
        <Spacing />
        <Paragraph size='sm'>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias atque
          omnis quibusdam repellat. Aut dolor ipsum maiores officiis saepe
          ullam? Aut blanditiis cumque dolores, ea earum ipsam iusto sunt
          voluptate!
        </Paragraph>
        <Spacing />
        <OptIn
          onChange={e => {
            this.onChangeHandler(e);
          }}
          checked={this.state.checked}
          text={
            <Paragraph >Aceito os termos de uso</Paragraph>
          }
        />
    </View>
    <View>
        <Spacing />
        <TextLink
            text='Link para documentação'
            onClick={() => {
            window.open('https://ame-miniapp-components.calindra.com.br/docs/optin/')
            }}
        />
    </View>
</Window>